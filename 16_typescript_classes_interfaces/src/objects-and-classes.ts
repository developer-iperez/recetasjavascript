import { Car } from './classes/car';
import { CarWithAccessControl } from './classes/carWithAccessControl';

function objectBasics() {
    let singleCar: { model: string, year: number, price: number } = {
        model: 'Ferrari 488 Pista',
        year: 2018,
        price: 270000
    };

    console.log('Single car inrformation:');
    console.log(`Model: ${singleCar.model}, Year: ${singleCar.year}, Price: ${singleCar.price}`);

    let carsCollection: { model: string, year: number, price: number, color?: string, priceWithTax (tax: number): number }[] = [{
        model: 'Mclaren 600 LT',
        year: 2018,
        price: 300000,
        priceWithTax: (tax) => {
            return 300000 * tax;
        }
    }, {
        model: 'Porsche 911 GT2 RS',
        year: 2018,
        price: 180000,
        priceWithTax: (tax) => {
            return 180000 * tax;
        }
    }, {
        model: 'Lamborghini Huracan Evo',
        year: 2020,
        price: 270000,
        color: 'yellow',
        priceWithTax: (tax) => {
            return 270000 * tax;
        }
    }];

    console.log('Car collection information:');
    carsCollection.forEach((car) => {
        console.log(`Model: ${car.model}, Year: ${car.year}, Price: ${car.price}, Color: ${car.color || 'N/A'}, Price with tax: ${car.priceWithTax(1.2)}`);
    });
}

function classesBasics() {
    let carsCollection: Car[] = [
        new Car('Mclaren 600 LT', 2018, 300000),
        new Car('Porsche 911 GT2 RS', 2018, 180000),
        new Car('Lamborghini Huracan Evo', 2020, 270000),
    ];

    carsCollection[2].color = 'yellow';

    console.log('Car collection information:');
    carsCollection.forEach((car) => {
        console.log(`Model: ${car.model}, Year: ${car.year}, Price: ${car.price}, Color: ${car.color || 'N/A'}, Price with tax: ${car.priceWithTax()}`);
    });
}

function classesWithAccessControl() {
    let car: CarWithAccessControl = new CarWithAccessControl(1, 'Mclaren 600 LT', 2018, 300000);
    car.setColor('yellow');
    console.log(`Id: ${car.id}, Model: ${car.model}, Year: ${car.year}, Price: ${car.price},  Color: ${car.getColor() || 'N/A'}, Price with tax: ${car.priceWithTax()}`);
}

export function objectAndClassesExamples() {
    console.log('TypeScript Object and Classes Examples\n-------------------');

    console.log('\nObject basics:');
    objectBasics();

    console.log('\nClasses basics:');
    classesBasics();

    console.log('\nClasses with access control:');
    classesWithAccessControl();
}
