import { Position } from "./interfaces/Position";
import { Drawable } from "./interfaces/Drawable";
import { Square } from "./classes/Square";
import { Circle } from "./classes/Circle";
import { MoveableSquare } from "./classes/MoveableSquare";

function basicInterfaceExample() {
    let position: Position = {
        x: 0,
        y: 0
    };
    console.log(`Position => x: ${position.x}, y: ${position.y}`);
}

function basicClassInterfaceExample() {
    let shapes: Drawable[] = [
        new Square({ x: 0, y: 0 }, { width: 100, height: 100 }, 'blue'),
        new Circle({ x: 50, y: 50 }, 50, 'white')
    ];
    
    for(const shape of shapes) {
        shape.paint();
    }
}

function extendedClassInterfacesExample() {
    let position: Position = { x: 200, y: 200 };
    let square: MoveableSquare = new MoveableSquare({ x: 0, y: 0 }, { width: 50, height: 50 }, 'black');
    square.move(position);
    square.paint();
    square.select();
    square.move(position);
    square.paint();
}

export function interfacesExamples() {
    console.log('\nTypeScript Interfaces sExamples\n-------------------');

    console.log('\nBasic interface:');
    basicInterfaceExample();

    console.log('\nBasic class and interface:');
    basicClassInterfaceExample();

    console.log('\Extended class and interfaces:');
    extendedClassInterfacesExample();
}