import { VehiclePart } from "./classes/VehiclePart";
import { Wheel } from "./classes/Wheel";
import { Break } from "./classes/Break";
import { Battery } from "./classes/Battery";

function abtractClasses() {
    let vehicleParts: VehiclePart[] = [
        new Wheel(4),
        new Break(2),
        new Battery(1)
    ];

    console.log('Vehicle parts to be repaired:');
    vehicleParts.forEach((part) => {
        console.log(`${part.name()}, total = ${part.total}`);
    });
}

export function abstractClassesExamples() {
    console.log('\nTypeScript Abstract Classes and Interfaces sExamples\n-------------------');

    console.log('\nAbstract classes:');
    abtractClasses();
}
