import { objectAndClassesExamples } from './objects-and-classes';
import { abstractClassesExamples } from './abstract-classes';
import { interfacesExamples } from './interfaces';

objectAndClassesExamples();
abstractClassesExamples();
interfacesExamples();
