export interface Drawable {
    paint(): void;
}
