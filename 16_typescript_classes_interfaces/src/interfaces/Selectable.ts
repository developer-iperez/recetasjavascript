export interface Selectable {
    select(): void;
}
