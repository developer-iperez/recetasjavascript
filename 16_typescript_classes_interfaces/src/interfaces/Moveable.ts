import { Position } from "./Position";
import { Selectable } from "./Selectable";

export interface Moveable extends Selectable {
    move(newPosition: Position): void;
}
