import { Position } from "../interfaces/Position";

export class Shape {

    protected position: Position;

    constructor(position: Position) {
        this.position = position;
    }

    toString(): string {
        return `[Position] => x: ${this.position.x}, y: ${this.position.y}`;
    }
}
