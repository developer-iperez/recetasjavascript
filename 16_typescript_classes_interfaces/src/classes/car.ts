export class Car {
    model: string;
    year: number;
    price: number;
    color: string;

    constructor(model: string, year: number, price: number) {
        this.model = model;
        this.year = year;
        this.price = price;
        this.color = 'N/A';
    }

    priceWithTax(tax: number = 1.2): number {
        return this.price * tax;
    }
}
