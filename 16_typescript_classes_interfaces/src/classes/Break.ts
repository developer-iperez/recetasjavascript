import { VehiclePart } from "./VehiclePart";
export class Break extends VehiclePart {
    public name(): string {
        return 'Break';
    }
}
