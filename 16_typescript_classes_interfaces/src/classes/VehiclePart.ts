export abstract class VehiclePart {
    public total: number;
    public abstract name(): string;

    constructor(total: number = 0) {
        this.total = total;
    }
}
