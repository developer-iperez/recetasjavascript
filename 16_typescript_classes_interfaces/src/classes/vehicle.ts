export class Vehicle {

    private color: string;

    constructor(public readonly id: number) {
        this.id = id;
        this.color = 'N/A';
    }

    public getColor(): string {
        return this.color;
    }

    public setColor(color: string): void {
        this.color = color;
    }
}
