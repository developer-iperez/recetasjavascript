import { VehiclePart } from "./VehiclePart";
export class Battery extends VehiclePart {
    public name(): string {
        return 'Battery';
    }
}
