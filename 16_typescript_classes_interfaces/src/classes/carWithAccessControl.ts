import { Vehicle } from './vehicle';

export class CarWithAccessControl extends Vehicle {
    public model: string;
    public year: number;
    public price: number;
    
    constructor(public readonly id: number, model: string, year: number, price: number) {
        super(id);
        this.model = model;
        this.year = year;
        this.price = price;
    }  

    public priceWithTax(tax: number = 1.2): number {
        return this.price * tax;
    }
}