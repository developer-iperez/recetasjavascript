import { Position } from "../interfaces/Position";
import { Drawable } from "../interfaces/Drawable";
import { Shape } from "./Shape";

export class Circle extends Shape implements Drawable {

    private radius: number;
    private color: string;

    constructor(position: Position, radius: number, color: string) {
        super(position);
        this.radius = radius;
        this.color = color;
    }

    paint(): void {
        console.log(`Painting circle... ${this.toString()}`);
    }

    toString(): string {
        let str = `${super.toString()}`;
        str += `, [Radius] => ${this.radius}`;
        str += `, [Color] => ${this.color}`;
        return str;
    }
}
