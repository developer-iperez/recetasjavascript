import { Position } from "../interfaces/Position";
import { Size } from "../interfaces/Size";
import { Drawable } from "../interfaces/Drawable";
import { Shape } from "./Shape";

export class Square extends Shape implements Drawable {

    private size: Size;
    private color: string;

    constructor(position: Position, size: Size, color: string) {
        super(position);
        this.size = size;
        this.color = color;
    }

    paint(): void {
        console.log(`Painting square... ${this.toString()}`);
    }

    toString(): string {
        let str = `${super.toString()}`;
        str += `, [Size] => Width: ${this.size.width}, Height: ${this.size.height}`;
        str += `, [Color] => ${this.color}`;
        return str;
    }
}
