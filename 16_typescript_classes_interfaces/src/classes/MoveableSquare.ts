import { Position } from "../interfaces/Position";
import { Size } from "../interfaces/Size";
import { Drawable } from "../interfaces/Drawable";
import { Moveable } from "../interfaces/Moveable";
import { Shape } from "./Shape";

export class MoveableSquare extends Shape implements Drawable, Moveable {

    private size: Size;
    private color: string;
    protected selected: boolean;

    constructor(position: Position, size: Size, color: string) {
        super(position);
        this.size = size;
        this.color = color;
        this.selected = false;
    }

    paint(): void {
        console.log(`Painting square... ${this.toString()}`);
    }

    select(): void {
        this.selected = !this.selected;
    }

    move(newPosition: Position): void {
        if (!this.selected) {
            console.log('[WARNING] The shape must be selected!');
        }
        else {
            this.position = newPosition;
        }
    }

    toString(): string {
        let str = `${super.toString()}`;
        str += `, [Selected] => ${this.selected}`;
        return str;
    }
}
