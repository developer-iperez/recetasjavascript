import { VehiclePart } from "./VehiclePart";
export class Wheel extends VehiclePart {
    constructor(total: number) {
        super(total);
    }


    public name(): string {
        return 'Wheel';
    }
}
