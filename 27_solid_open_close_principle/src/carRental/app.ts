import VehicleRentalReport from "./application/VehicleRentalReport"
import VehicleRentalService from "./application/VehicleRentalService"
import Car from "./domain/Car"
import Client from "./domain/Client"
import Motorbike from "./domain/Motorbike"
import RentalBill from "./domain/RentalBill"
import RentalInformation from "./domain/RentalInformation"

function calculateCarRentalDemo() {
    const car = new Car()
    car.model = 'SuperCar'
    car.date = new Date(2015, 4, 1)
    car.color = 'black'
    car.licenseNumber = 'ABC1234'

    const client = new Client()
    client.age = 20 

    const carRentalInformation = new RentalInformation()
    carRentalInformation.rentalDays = 3

    const vehicleRentalService = new VehicleRentalService()
    const carRentalBill: RentalBill = vehicleRentalService.calculate(car, client, carRentalInformation)

    const carRentalReport = new VehicleRentalReport(car, client, carRentalBill)
    carRentalReport.print()
}

function calculateMotorbikeRentalDemo() {
    const motorbike = new Motorbike()
    motorbike.model = 'SuperBike'
    motorbike.date = new Date(2015, 4, 1)
    motorbike.color = 'black'
    motorbike.licenseNumber = 'ABC56789'

    const client = new Client()
    client.age = 20

    const motorbikeRentalInformation = new RentalInformation()
    motorbikeRentalInformation.rentalDays = 3

    const vehicleRentalService = new VehicleRentalService()
    const motorbikeRentalBill: RentalBill = vehicleRentalService.calculate(motorbike, client, motorbikeRentalInformation)

    const motorbikeRentalReport = new VehicleRentalReport(motorbike, client, motorbikeRentalBill)
    motorbikeRentalReport.print()
}

export default function() {
    calculateCarRentalDemo()
    calculateMotorbikeRentalDemo()
}