import CarRentalBill from "../domain/RentalBill";
import Client from "../domain/Client";
import Vehicle from "../domain/Vehicle";

export default class VehicleRentalReport {

    private vehicle: Vehicle
    private client: Client
    private rentalBill: CarRentalBill

    constructor(vehicle: Vehicle, client: Client, rentalBill: CarRentalBill) {
        this.vehicle = vehicle
        this.client = client
        this.rentalBill = rentalBill
    }

    print() {
        let report = ''

        report += `Rental vehicle report\n`
        report += `-----------------\n`
        report += `- Vehicle information:\n`
        report += `     - Type: ${this.vehicle.type}\n`
        report += `     - Model: ${this.vehicle.model}\n`
        report += `     - Date: ${this.vehicle.date}\n`
        report += `     - Color: ${this.vehicle.color}\n`
        report += `     - License number: ${this.vehicle.licenseNumber}\n`
        report += `\n`

        report += `- Vehicle extras:\n`
        if (this.vehicle.getExtras().length === 0) {
            report += `     - No extras\n`
        } else {
            for (const extra of this.vehicle.getExtras()) {
                report += `     - ${extra}\n`
            }
        }
        report += `\n`

        report += `- Client information:\n`
        report += `     - Age: ${this.client.age}\n`
        report += `\n`
        report += `- Rental price:\n`
        report += `     - Total rental days: ${this.rentalBill.rentalDays}\n`
        report += `     - Price by day: ${this.rentalBill.priceByDay}€\n`
        report += `     - Insurance price: ${this.rentalBill.insurancePrice}€\n`
        report += `     - Total: ${this.rentalBill.total}€\n`

        console.log(report)
    }

}