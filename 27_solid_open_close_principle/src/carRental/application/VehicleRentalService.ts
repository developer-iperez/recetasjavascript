import VehicleRentalBill from "../domain/RentalBill.js"
import Client from "../domain/Client.js"
import RentalInformation from "../domain/RentalInformation.js"
import Vehicle from "../domain/Vehicle.js"
import VehicleInsurancePrice from "./VehicleInsurancePrice.js"
import VehicleRentalPrice from "./VehicleRentalPrice.js"

export default class VehicleRentalService {

    calculate(vehicle: Vehicle, client: Client, rentalInformation: RentalInformation): VehicleRentalBill {
        const carRentalPriceByDay = new VehicleRentalPrice()
        const carInsurancePrice = new VehicleInsurancePrice()
        const rentalBill = new VehicleRentalBill()

        rentalBill.priceByDay = carRentalPriceByDay.calculate(vehicle)
        rentalBill.insurancePrice = carInsurancePrice.calculate(vehicle, client)
        rentalBill.rentalDays = rentalInformation.rentalDays
        rentalBill.total = (rentalBill.rentalDays * rentalBill.priceByDay) + rentalBill.insurancePrice

        return rentalBill
    }

}