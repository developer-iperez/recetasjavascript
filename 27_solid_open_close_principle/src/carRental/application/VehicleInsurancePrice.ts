import Client from "../domain/Client";
import Vehicle from "../domain/Vehicle";

export default class VehicleInsurancePrice {

    calculate(vehicle: Vehicle, client: Client): number {
        let insurancePrice = 50;
        const currentYear = new Date().getFullYear();
        const vehicleAgeInYears = currentYear - vehicle.date.getFullYear();

        if (vehicle.color === 'black') {
            insurancePrice += 50;
        }
        else if (vehicle.color === 'red' || vehicle.color === 'yellow') {
            insurancePrice += 10;
        }

        if (vehicle.model === 'Mercedes') {
            insurancePrice += 50;
        }

        if (vehicleAgeInYears <= 1) {
            insurancePrice += 30;
        }

        if (client.age < 25) {
            insurancePrice += 25;
        }

        return insurancePrice;
    }
    
}