import Vehicle from "../domain/Vehicle";

export default class VehicleRentalPrice {

    calculate(vehicle: Vehicle): number {
        const currentYear = new Date().getFullYear();
        const vehicleAgeInYears = currentYear - vehicle.date.getFullYear();

        if (vehicleAgeInYears <= 1) {
            return 100;
        }
        else if (vehicleAgeInYears > 1 && vehicleAgeInYears <= 5) {
            return 70;
        }
        else {
            return 40;
        }
    }

}