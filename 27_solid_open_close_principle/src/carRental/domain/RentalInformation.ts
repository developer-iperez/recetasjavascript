export default class RentalInformation {

    private _rentalDays: number

    constructor() {
        this._rentalDays = 0
    }

    get rentalDays() {
        return this._rentalDays
    }

    set rentalDays(rentalDays: number) {
        this._rentalDays = rentalDays
    }
}