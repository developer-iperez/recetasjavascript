import greedyRecursive from './greedyCoinChange.js'

export class CoinChangeProbleResult {
    constructor(articlePrice, customerPayment, changeRelationship) {
        this.articlePrice = articlePrice
        this.customerPayment = customerPayment
        this.totalChange = this.customerPayment - this.articlePrice
        this.changeRelationship = changeRelationship
    }

    toString() {
        let str = `The customer pays ${this.customerPayment} and the item is worth ${this.articlePrice}.\n`
        str += `The change to be returned is ${this.totalChange}.\n`
        
        for (let i = 0; i < this.changeRelationship.length; i++) {
            str += `\t- Coin value ${this.changeRelationship[i].coinValue}=${this.changeRelationship[i].total}\n`
        }

        return str
    }
}

export class CoinChangeProbleUseCase {
    constructor(availableCoins) {
        this.availableCoins = availableCoins
    }

    calculateChange(articlePrice, customerPayment) {
        const totalChange = customerPayment - articlePrice
        const totalChangeResult = greedyRecursive(this.availableCoins, totalChange)
        let changeRelationship = []

        for (let i = 0; i < totalChangeResult.length; i++) {
            changeRelationship.push({
                coinValue: this.availableCoins[i],
                total: totalChangeResult[i]
            })
        }

        return new CoinChangeProbleResult(articlePrice, customerPayment, changeRelationship);
    }
}