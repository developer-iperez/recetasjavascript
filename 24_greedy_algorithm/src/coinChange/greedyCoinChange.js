// Selection function
function isOptimalResult(rest, coin) {
    return rest - coin >= 0
}

// Recursive Greedy method
function greedy(availableCoinsList, indexOfOptimnalCoinInAvailableCoinsList, rest, result) {
    if (rest > 0) {
        if (isOptimalResult(rest, availableCoinsList[indexOfOptimnalCoinInAvailableCoinsList])) {
            result[indexOfOptimnalCoinInAvailableCoinsList]++
            rest -= availableCoinsList[indexOfOptimnalCoinInAvailableCoinsList]
        }
        else 
        indexOfOptimnalCoinInAvailableCoinsList += 1

        greedy(availableCoinsList, indexOfOptimnalCoinInAvailableCoinsList, rest, result)
    }
}

// Main method
export default function greedyRecursive(availableCoinsList, total) {
    let result = availableCoinsList.map(x => 0)
    const indexOfOptimnalCoinInAvailableCoinsList = 0
    greedy(availableCoinsList, indexOfOptimnalCoinInAvailableCoinsList, total, result)

    return result
}