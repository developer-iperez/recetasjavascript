import { CoinChangeProbleUseCase } from './coinChange/coinChangeProblemUseCase.js'
import { ScheduleBuilder, ActivitySelectionSchedulerUseCase } from './activitySelectionScheduler/activitySelectionSchedulerUseCase.js'

////////////////////////////////////////////////////////////////
// Example coin change recursive

const articlePrice = 37
const customerPayment = 50
const availableCoins = [50, 20, 10, 5, 1]

const coinChangeProbleUseCase = new CoinChangeProbleUseCase(availableCoins)
const coinChangeProbleResult = coinChangeProbleUseCase.calculateChange(articlePrice, customerPayment)

console.log('////////////////////////')
console.log('// Coin Change Problem\n')
console.log(coinChangeProbleResult.toString());

////////////////////////////////////////////////////////////////
// Example activity schedules recursive - The activity selection problem is characteristic to this class of problems, where the goal is to pick the maximum number of activities that do not clash with each other.

const scheduleBuilder = new ScheduleBuilder()
let schedules = []

schedules.push(scheduleBuilder.build('T1', 11, 0, 11, 30))
schedules.push(scheduleBuilder.build('T2', 13, 30, 15, 0))
schedules.push(scheduleBuilder.build('T3', 11, 10, 12, 0))
schedules.push(scheduleBuilder.build('T4', 12, 0, 14, 0))
schedules.push(scheduleBuilder.build('T5', 10, 0, 11, 5))
schedules.push(scheduleBuilder.build('T6', 12, 30, 13, 0))

const activitySelectionSchedulerUseCase = new ActivitySelectionSchedulerUseCase()
const activitySelectionSchedulerResult = activitySelectionSchedulerUseCase.calculateSchedules(schedules)

console.log('/////////////////////////////////////////')
console.log('// Activity Selection Scheduler Problem\n')
console.log(activitySelectionSchedulerResult.toString())
