import { GreedySchedule, ScheduleTask, ScheduleTaskSchedule, ScheduleTime } from './greedyScheduling.js'

export class ScheduleBuilder {
    build(title, hourStart, minuteStart, hourEnd, minuteEnd) {
        return new ScheduleTask(title, new ScheduleTaskSchedule(new ScheduleTime(hourStart, minuteStart), new ScheduleTime(hourEnd, minuteEnd)))
    }
}

export class ActivitySelectionSchedulerResult {
    constructor(results) {
        this.results = results || []
    }

    _formatNumber(number) {
        return `${('0' + number).slice(-2)}`;
    }

    toString() {
        let str = `Activities scheduled for today:\n`

        for (const result of this.results) {
            const start = `${this._formatNumber(result.start.hour)}:${this._formatNumber(result.start.minute)}`
            const end = `${this._formatNumber(result.end.hour)}:${this._formatNumber(result.end.minute)}`
            str += `\t- [${start} to ${end}] ${result.title}\n`
        }

        return str
    }
}

export class ActivitySelectionSchedulerUseCase {
    calculateSchedules(schedules) {
        const greedySchedule = new GreedySchedule(schedules)
        const scheduleResult = greedySchedule.getScheduleTasks()

        const results = []

        for (const scheduleItem of scheduleResult) {
            results.push({
                title: scheduleItem.title, 
                start: scheduleItem.schedule.start,
                end: scheduleItem.schedule.end
            })
        }

        return new ActivitySelectionSchedulerResult(results)
    }
}