export class ScheduleTime {
    constructor(hour, minute) {
        this.hour = hour
        this.minute = minute
    }

    compareTo(scheduleTime) {
        if (!scheduleTime)
            return 1

        const hourDifference = this.hour - scheduleTime.hour
        if (hourDifference !== 0)
            return hourDifference
        
        const minuteDifference = this.minute - scheduleTime.minute
        return minuteDifference
    }
}

export class ScheduleTaskSchedule {
    constructor(start, end) {
        this.start = start
        this.end = end
    }

    compareTo(schedule) {
        return this.end.compareTo(schedule.end)
    }
}

export class ScheduleTask {
    constructor(title, schedule) {
        this.title = title
        this.schedule = schedule
    }
}

export class GreedySchedule {
    constructor(scheduleList) {
        this.scheduleList = scheduleList.slice()

        // Sort schedules by the end time, ascendent
        this.scheduleList.sort((s, t) => {
            return s.schedule.compareTo(t.schedule)
        })
    }

    getScheduleTasks() {
        let result = []
        const indexOfOptimnalSchedule = 0
        this._greedyIntervalSchedule(result, indexOfOptimnalSchedule, null)

        return result
    }

    _greedyIntervalSchedule(result, indexOfOptimnalSchedule, lastEndTime) {
        if (indexOfOptimnalSchedule < this.scheduleList.length) {
            if (this._isOptimalResult(this.scheduleList[indexOfOptimnalSchedule].schedule, lastEndTime)) {
                result.push(this.scheduleList[indexOfOptimnalSchedule])
                lastEndTime = this.scheduleList[indexOfOptimnalSchedule].schedule.end
            }

            this._greedyIntervalSchedule(result, ++indexOfOptimnalSchedule, lastEndTime)
        }
    }

    // Selection function
    _isOptimalResult(schedule, lastEndTime) {
        return schedule.start.compareTo(lastEndTime) >= 1
    }
}