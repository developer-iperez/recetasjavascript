export default class Car {

    private _date: Date
    private _model: string
    private _color: string
    private _licenseNumber: string

    constructor() {
        this._date = new Date()
        this._model = ''
        this._color = ''
        this._licenseNumber = ''
    }

    get date() {
        return this._date;
    }

    set date(date: Date) {
        this._date = date;
    }

    get model() {
        return this._model;
    }

    set model(model: string) {
        this._model = model;
    }

    get color() {
        return this._color;
    }

    set color(color: string) {
        this._color = color;
    }

    get licenseNumber() {
        return this._licenseNumber;
    }

    set licenseNumber(licenseNumber: string) {
        this._licenseNumber = licenseNumber;
    }
}