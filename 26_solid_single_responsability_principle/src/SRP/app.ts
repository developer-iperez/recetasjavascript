import Car from "./Car"
import CarRentalBill from "./CarRentalBill"
import CarRentalInformation from "./CarRentalInformation"
import CarRentalReport from "./CarRentalReport"
import CarRentalService from "./CarRentalService"
import Client from "./Client"

function calculateCarRentalWithSRP() {
    const car = new Car()
    car.model = 'SuperCar'
    car.date = new Date(2015, 4, 1)
    car.color = 'black'
    car.licenseNumber = 'ABC1234'

    const client = new Client()
    client.age = 20 

    const carRentalInformation = new CarRentalInformation()
    carRentalInformation.rentalDays = 3

    const carRentalService = new CarRentalService()
    const carRentalBill: CarRentalBill = carRentalService.calculate(car, client, carRentalInformation)

    const carRentalReport = new CarRentalReport(car, client, carRentalBill)
    carRentalReport.print()
}

export default function() {
    calculateCarRentalWithSRP()
}