import Car from "./Car";
import CarRentalBill from "./CarRentalBill";
import Client from "./Client";

export default class CarRentalReport {

    private car: Car
    private client: Client
    private carRentalBill: CarRentalBill

    constructor(car: Car, client: Client, carRentalBill: CarRentalBill) {
        this.car = car
        this.client = client
        this.carRentalBill = carRentalBill
    }

    print() {
        let report = ''

        report += `Rental car report\n`;
        report += `-----------------\n`;
        report += `- Car information:\n`;
        report += `     - Model: ${this.car.model}\n`;
        report += `     - Date: ${this.car.date}\n`;
        report += `     - Color: ${this.car.color}\n`;
        report += `     - License number: ${this.car.licenseNumber}\n`;
        report += `\n`;
        report += `- Client information:\n`;
        report += `     - Age: ${this.client.age}\n`;
        report += `\n`;
        report += `- Rental price:\n`;
        report += `     - Total rental days: ${this.carRentalBill.rentalDays}\n`;
        report += `     - Price by day: ${this.carRentalBill.priceByDay}€\n`;
        report += `     - Insurance price: ${this.carRentalBill.insurancePrice}€\n`;
        report += `     - Total: ${this.carRentalBill.total}€\n`;

        console.log(report)
    }

}