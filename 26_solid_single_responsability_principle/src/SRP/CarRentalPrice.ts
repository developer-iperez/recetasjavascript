import Car from "./Car";

export default class CarRentalPrice {

    calculate(car: Car): number {
        const currentYear = new Date().getFullYear();
        const carAgeInYears = currentYear - car.date.getFullYear();

        if (carAgeInYears <= 1) {
            return 100;
        }
        else if (carAgeInYears > 1 && carAgeInYears <= 5) {
            return 70;
        }
        else {
            return 40;
        }
    }

}