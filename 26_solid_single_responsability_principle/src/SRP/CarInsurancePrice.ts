import Car from "./Car";
import Client from "./Client";

export default class CarInsurancePrice {

    calculate(car: Car, client: Client): number {
        let insurancePrice = 50;
        const currentYear = new Date().getFullYear();
        const carAgeInYears = currentYear - car.date.getFullYear();

        if (car.color === 'black') {
            insurancePrice += 50;
        }
        else if (car.color === 'red' || car.color === 'yellow') {
            insurancePrice += 10;
        }

        if (car.model === 'Mercedes') {
            insurancePrice += 50;
        }

        if (carAgeInYears <= 1) {
            insurancePrice += 30;
        }

        if (client.age < 25) {
            insurancePrice += 25;
        }

        return insurancePrice;
    }
    
}