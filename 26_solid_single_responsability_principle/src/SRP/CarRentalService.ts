import Car from "./Car.js"
import CarInsurancePrice from "./CarInsurancePrice.js"
import CarRentalBill from "./CarRentalBill.js"
import CarRentalInformation from "./CarRentalInformation.js"
import CarRentalPrice from "./CarRentalPrice.js"
import Client from "./Client.js"

export default class CarRentalService {

    calculate(car: Car, client: Client, rentalInformation: CarRentalInformation): CarRentalBill {
        const carRentalPriceByDay = new CarRentalPrice()
        const carInsurancePrice = new CarInsurancePrice()
        const rentalBill = new CarRentalBill()

        rentalBill.priceByDay = carRentalPriceByDay.calculate(car)
        rentalBill.insurancePrice = carInsurancePrice.calculate(car, client)
        rentalBill.rentalDays = rentalInformation.rentalDays
        rentalBill.total = (rentalBill.rentalDays * rentalBill.priceByDay) + rentalBill.insurancePrice

        return rentalBill
    }

}