import CarRental from './CarRental'

function calculateCarRentalWithoutSRP() {
    const carRental = new CarRental()
    carRental.model = 'SuperCar'
    carRental.date = new Date(2015, 4, 1)
    carRental.color = 'black'
    carRental.licenseNumber = 'ABC1234'
    carRental.clientAge = 20
    carRental.rentalDays = 3
    
    carRental.printRentalReport()
}

export default function() {
    calculateCarRentalWithoutSRP()    
}
