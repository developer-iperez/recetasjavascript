export default class CarRental {

    private _date: Date
    private _model: string
    private _color: string
    private _licenseNumber: string
    private _clientAge: number
    private _rentalDays: number

    constructor() {
        this._date = new Date()
        this._model = ''
        this._color = ''
        this._licenseNumber = ''
        this._clientAge = 20
        this._rentalDays = 0
    }

    get date() {
        return this._date;
    }

    set date(date) {
        this._date = date;
    }

    get model() {
        return this._model;
    }

    set model(model) {
        this._model = model;
    }

    get color() {
        return this._color;
    }

    set color(color) {
        this._color = color;
    }

    get licenseNumber() {
        return this._licenseNumber;
    }

    set licenseNumber(licenseNumber) {
        this._licenseNumber = licenseNumber;
    }

    get clientAge() {
        return this._clientAge;
    }

    set clientAge(clientAge) {
        this._clientAge = clientAge;
    }

    get rentalDays() {
        return this._rentalDays
    }

    set rentalDays(rentalDays) {
        this._rentalDays = rentalDays
    }

    calculateRentPrice() {
        const currentYear = new Date().getFullYear();
        const carAgeInYears = currentYear - this._date.getFullYear();

        if (carAgeInYears <= 1) {
            return 100;
        }
        else if (carAgeInYears > 1 && carAgeInYears <= 5) {
            return 70;
        }
        else {
            return 40;
        }
    }

    calculatePriceOfCarInsurance() {
        let insurancePrice = 50;
        const currentYear = new Date().getFullYear();
        const carAgeInYears = currentYear - this._date.getFullYear();

        if (this.color === 'black') {
            insurancePrice += 50;
        }
        else if (this.color === 'red' || this.color === 'yellow') {
            insurancePrice += 10;
        }

        if (this.model === 'Mercedes') {
            insurancePrice += 50;
        }

        if (carAgeInYears <= 1) {
            insurancePrice += 30;
        }

        if (this.clientAge < 25) {
            insurancePrice += 25;
        }

        return insurancePrice;
    }

    printRentalReport() {
        let report = '';
        const rentPrice = this.calculateRentPrice();
        const insurancecPrice = this.calculatePriceOfCarInsurance();

        report += `Rental car report\n`;
        report += `-----------------\n`;
        report += `- Car information:\n`;
        report += `     - Model: ${this.model}\n`;
        report += `     - Date: ${this.date}\n`;
        report += `     - Color: ${this.color}\n`;
        report += `     - License number: ${this.licenseNumber}\n`;
        report += `\n`;
        report += `- Client information:\n`;
        report += `     - Age: ${this.clientAge}\n`;
        report += `\n`;
        report += `- Rental price:\n`;
        report += `     - Total rental days: ${this.rentalDays}\n`;
        report += `     - Price by day: ${rentPrice}€\n`;
        report += `     - Insurance price: ${insurancecPrice}€\n`;
        report += `     - Total: ${(this.rentalDays * rentPrice) + insurancecPrice}€\n`;

        console.log(report)
    }

}
