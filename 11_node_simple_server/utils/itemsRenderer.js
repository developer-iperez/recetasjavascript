module.exports.toHtml = (list) => {
    let html = [];

    html.push('<table>');
    for(let i = 0; i < list.length; i++) {
        html.push('<tr>');
        html.push('<td>');
        html.push('<form method="GET" action="/update">');
        html.push(`<input type="text" name="item" value="${list[i]}">`);
        html.push(`<input type="hidden" name="oldItem" value="${list[i]}">`);
        html.push(`&nbsp;<input type="submit" value="Update">`);
        html.push('</form>');
        html.push('</td>');
        html.push('<td>');
        html.push('<form method="GET" action="/remove">');
        html.push(`<input type="hidden" name="item" value="${list[i]}">`);
        html.push('<input type="submit" value="Remove">');
        html.push('</form>');
        html.push('</td>');
        html.push('</tr>');
    }
    html.push('</table>');

    return html.join('');
};
