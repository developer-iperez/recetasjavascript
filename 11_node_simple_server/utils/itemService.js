let list = [];

let itemsService = {
    list: () => {
        return list;
    },

    add: (item) => {
        if (item !== null && item !== undefined && item !== '') {
            list.push(item);
            console.log(`Added new item: ${item}`);
        }
    },

    update: (oldItem, newItem) => {
        let index = list.indexOf(oldItem);
        if (index !== -1) {
            list[index] = newItem;
        }
    },

    remove: (item) => {
        for (let i = 0; i < list.length; i++) {
            if (list[i] === item) {
                list.splice(i, 1);
            }
        }
    }
}

module.exports = itemsService;
