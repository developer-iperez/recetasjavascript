// Importación de librerías
var http = require('http');
var url = require('url');
var fs = require('fs');
var itemsService = require('./utils/itemService');
var itemsRenderer = require('./utils/itemsRenderer');

function sendResponse(response, code, contentType, content) {
    response.writeHead(code, { 'Content-Type': contentType });
    response.write(content, 'utf-8');
    response.end();
}

// Creamos la instancia del servidor
const server = http.createServer((request, response) => {
    let q = url.parse(request.url, true);
    let path = q.pathname;
    let item;
    let requestPath = __dirname + '/pages/index.html';
    let contentType = 'text/html';

    switch(path) {
        case '/favicon.ico':
            requestPath = __dirname + '/favicon.ico';
            contentType = 'image/x-icon';

            break;

        case '/':
            break;

        case '/create':
            item = q.query.item || null;
            itemsService.add(item);

            break;

        case '/update':
            item = q.query.item || null;
            oldItem = q.query.oldItem || null;
            itemsService.update(oldItem, item);

            break;

        case '/remove':
            item = q.query.item || null;
            itemsService.remove(item);

            break;
    }
    
    fs.readFile(requestPath, (error, fileContent) => {
        if (error) {
            if(error.code == 'ENOENT'){
                fs.readFile(__dirname + '/pages/404.html', (error, html404) => {
                    // En este caso he decido devolver una página HTML con el error 404, por eso devuelvo el código 200
                    sendResponse(response, 200, contentType, html404);
                });
            }
            else {
                // Informamos que se ha producido un error interno en el servidor
                sendResponse(response, 500, contentType, 'Error on process the request');
            }
        }
        else {
            if (contentType === 'text/html') {
                const list = itemsService.list();
                const listHtml = itemsRenderer.toHtml(list);

                fileContent = fileContent.toString();
                // Uso una expresión regular para reemplazar el tag DIV de la página por el contenido generado.
                fileContent = fileContent.replace(/<div>(.*)<\/div>/, listHtml);
            }
    
            // Escribimos las cabeceras de la respuesta
            sendResponse(response, 200, contentType, fileContent);
        }
    });
});

// Empezamos a escuchar en el puerto definido, en este caso 3000
server.listen(3000);
