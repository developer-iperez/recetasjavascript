// The assert module provides a set of assertion functions for verifying invariants.
// Link: https://nodejs.org/api/assert.html#assert_assert
import assert from 'assert';
import Database from '../database/users.database.lowdb.js';
import User from '../model/user.js'

describe('LowDB database integration test', function() {

    this.database = null;

    beforeEach(function() {
        this.database = new Database('test.db.json');
        this.database.reset();
    });

    it('Database constructor cannot receive a undefined or null filename', function() {
        assert.throws(function() {              // Expects the function to throw an error.
            new Database(null);
        }, {
            name: 'Error',
            message: 'Filename cannot be undefined or null'
        });
    });

    it('The default user list is empty when create the database and reset', function() {
        const users = this.database.list();

        assert.ok(users);                       // Tests if value is truthy.
        assert.strictEqual(users.length, 0);    // Tests strict equality between the actual and expected parameters 
    });

    it('Insert a new user, the list must be not empty and the user id must be the correct one', function() {
        const id = '1';
        const username = 'michael';
        const password = 'michaelpass';
        
        this.database.insert(id, new User(id, username, password));
        const users = this.database.list();

        assert.ok(users);
        assert.strictEqual(users.length, 1);
        assert.strictEqual(users[0].id, id);
    });

    it('Insert a new user twice, the list must have only one user and the user id must be the correct one', function() {
        const id = '1';
        const username = 'michael';
        const password = 'michaelpass';
        
        this.database.insert(id, new User(id, username, password));
        this.database.insert(id, new User(id, username, password));
        const users = this.database.list();

        assert.ok(users);
        assert.strictEqual(users.length, 1);
        assert.strictEqual(users[0].id, id);
    });

    it('Insert a new user then get the user by id', function() {
        const id = '1';
        const username = 'michael';
        const password = 'michaelpass';
        
        this.database.insert(id, new User(id, username, password));
        const user = this.database.get(id);

        assert.ok(user);
        assert.strictEqual(user.id, id);
        assert.strictEqual(user.username, username);
        assert.strictEqual(user.password, password);
        assert.strictEqual(user.enabled, true);
    });

    it('Insert a new user then try to get the user by invalid id', function() {
        const id = '1';
        const username = 'michael';
        const password = 'michaelpass';
        
        this.database.insert(id, new User(id, username, password));
        const user = this.database.get('2');

        assert.ifError(user);                           // Throws value if value is not undefined or null
    });

    it('Update existing user', function() {
        const id = '1';
        const username = 'leachim';
        const password = 'michaelpass';
        
        this.database.insert(id, new User(id, username, password));
        this.database.update(id, new User(id, 'michael', password));
        const user = this.database.get('1');

        assert.ok(user);
        assert.strictEqual(user.username, 'michael');
    });

    it('Update non existing user', function() {
        const id = '1';
        const username = 'leachim';
        const password = 'michaelpass';
        
        this.database.insert(id, new User(id, username, password));
        this.database.update(2, new User(2, 'michael', password));
        const user = this.database.get('1');

        assert.ok(user);
        assert.strictEqual(user.username, username);
    });

    it('Remove existing user', function() {
        const id = '1';
        const username = 'michael';
        const password = 'michaelpass';
        
        this.database.insert(id, new User(id, username, password));
        this.database.remove(id);
        const user = this.database.get('1');

        assert.ifError(user);
    });

    it('Remove non existing user', function() {
        const id = '1';
        const username = 'michael';
        const password = 'michaelpass';
        
        this.database.insert(id, new User(id, username, password));
        this.database.remove('2');
        const user = this.database.get('1');

        assert.ok(user);
        assert.strictEqual(user.id, id);
    });
});