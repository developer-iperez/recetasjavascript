import low from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync.js';

class DatabaseLowDb {

    constructor(fileName) {
        this.adapter = new FileSync(fileName);
        this.dbName = 'users';

        // Database initialization
        this.db = low(this.adapter);
        this.db.defaults({ users: [] })
            .write();
    }

    list() {
        return this.db.get(this.dbName)
            .value();
    }

    get(id) {
        return this.db.get(this.dbName)
            .filter({ id })
            .value();
    }

    insert(id, username, password) {
        this.db.get(this.dbName)
            .push({
                id, 
                username,
                password,
                enabled: true
            })
            .write();
    }

    update(id, username, password, enabled) {
        this.db.get(this.dbName)
            .find({ id })
            .assign({
                username,
                password,
                enabled
            })
            .write();
    }

    remove(id) {
        this.db.get(this.dbName)
            .remove({ id })
            .write();
    }
}

export default DatabaseLowDb;
