import low from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync.js';

class DatabaseLowDb {

    constructor(fileName, dbName) {
        if (fileName === undefined || fileName === null)
            throw new Error('Filename cannot be undefined or null');    

        this.adapter = new FileSync(fileName);
        this.dbName = dbName;

        // Database initialization
        this._db = low(this.adapter);
        this._db.defaults({ users: [] })
            .write();
    }

    db() {
        return this._db.get(this.dbName);
    }

    list() {
        return this.db()
            .value();
    }

    get(id) {
        let item = this.db()
            .filter({ id })
            .take(1)
            .value();
        return (!!item && item.length > 0) ? item[0] : null;
    }

    insert(id, data) {
        if (this.exists(id))
            return;

        this.db()
            .push(data)
            .write();
    }

    update(id, data) {
        if (!this.exists(id))
            return;

        this.db()
            .find({ id })
            .assign(data)
            .write();
    }

    remove(id) {
        this.db()
            .remove({ id })
            .write();
    }

    exists(id) {
        const item = this.get(id);
        return !!item;
    }

    reset() {
        this.db()
            .remove()
            .write();
    }
}

export default DatabaseLowDb;
