import DatabaseLowDb from './database.lowdb.v2.js';
import User from '../model/user.js';

class UserDatabaseLowDb extends DatabaseLowDb {

    constructor(fileName) {
        super(fileName, 'users');
    }

    insert(id, user) {       
        if (!!user === false)
            throw new Error('User cannot be null');

        if (user instanceof User === false)
            throw new Error('User class is an invalid type');

        super.insert(id, user);
    }

    update(id, user) {
        if (!!user === false)
            throw new Error('User cannot be null');

        if (user instanceof User === false)
            throw new Error('User class is an invalid type');

        super.update(id, user);
    }
}

export default UserDatabaseLowDb;
