//import Database from './database/database.lowdb.v1.js';
import Database from './database/users.database.lowdb.js';
import User from './model/user.js';

//let database = new Database('mydb.v1.json');
let database = new Database('mydb.v2.json');
database.insert('1', new User('1', 'Michal', 'michaelpass'));
database.update('1', new User('1', 'Michael', 'michaelpass', true));
database.insert('2', new User('2', 'Joe', 'joepass'));
console.log(`User with id=1 : ${JSON.stringify(database.get('1'))}`);
console.log(`User list : ${JSON.stringify(database.list())}`);

database.remove('1');
database.insert('2', new User('1', 'Joe', 'joepass'));
console.log(`User list : ${JSON.stringify(database.list())}`);
