class User {
    constructor(id, username, password, enabled = true) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
    }
}

export default User;