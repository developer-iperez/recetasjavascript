function showSection(evt, sectionName) {
    let tabcontent = document.getElementsByClassName('section_content');
    let tablinks = document.getElementsByClassName('tablinks');

    for (let i = 0; i < tabcontent.length; i++) {
        tabcontent[i].className = tabcontent[i].className.replace(' active', '');
    }
    
    for (let i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(' active', '');
    }
    
    document.getElementById(sectionName).className += ' active';
    evt.currentTarget.className += ' active';
}

function showMessage(message) {
    const logContainer = document.getElementById('log-container');
    logContainer.innerHTML = message;
}

window.onload = () => {
    setTimeout(() => {
        showMessage('');
        document.getElementById('container').className = 'visible';
        document.getElementsByClassName('tablinks')[0].click();
    }, 2000);
};

showMessage('Loading the page. Please wait...');
