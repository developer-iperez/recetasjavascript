import { RentalApp } from './carRental/app'
import { MotorbikeInsurancePrice } from "./carRental/application/Motorbike/MotorbikeInsurancePrice"
import { CarInsurancePrice } from "./carRental/application/Car/CarInsurancePrice"
import { MotorbikeRentalPrice } from "./carRental/application/Motorbike/MotorbikeRentalPrice"
import { CarRentalPrice } from "./carRental/application/Car/CarRentalPrice"
import Car from './carRental/domain/Car'
import Client from './carRental/domain/Client'
import Motorbike from './carRental/domain/Motorbike'
import { RentalInformation } from './carRental/domain/RentalInformation'

function calculateCarRentalDemo() {
    const car = new Car()
    car.model = 'SuperCar'
    car.date = new Date(2015, 4, 1)
    car.color = 'black'
    car.licenseNumber = 'ABC1234'
    car.includesChildSeats = true

    const client = new Client()
    client.age = 20 

    const rentalInformation = new RentalInformation()
    rentalInformation.rentalDays = 3

    const carRentalPrice = new CarRentalPrice(car)
    const carInsurancePrice = new CarInsurancePrice(car, client)

    const rentalApp = new RentalApp(car, client, rentalInformation)
    rentalApp.calculateBillAndPrintReport(carRentalPrice, carInsurancePrice)
}

function calculateMotorbikeRentalDemo() {
    const motorbike = new Motorbike()
    motorbike.model = 'SuperBike'
    motorbike.date = new Date(2015, 4, 1)
    motorbike.color = 'black'
    motorbike.licenseNumber = 'ABC56789'

    const client = new Client()
    client.age = 20 

    const rentalInformation = new RentalInformation()
    rentalInformation.rentalDays = 3

    const motorbikeRentalPrice = new MotorbikeRentalPrice(motorbike)
    const motorbikeInsurancePrice = new MotorbikeInsurancePrice(motorbike, client)

    const rentalApp = new RentalApp(motorbike, client, rentalInformation)
    rentalApp.calculateBillAndPrintReport(motorbikeRentalPrice, motorbikeInsurancePrice)
}

calculateCarRentalDemo()
calculateMotorbikeRentalDemo()
