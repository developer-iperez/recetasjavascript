import { VehicleInsurancePrice } from "./domain/interfaces/VehicleInsurancePrice"
import { VehicleRentalPrice } from "./domain/interfaces/VehicleRentalPrice"
import { VehicleRentalReport } from "./application/Report/VehicleRentalReport"
import { VehicleRentalService } from "./application/Shared/VehicleRentalService"
import Client from "./domain/Client"
import { RentalBill } from "./domain/RentalBill"
import { RentalInformation } from "./domain/RentalInformation"
import Vehicle from "./domain/Vehicle"

export class RentalApp {

    private vehicle: Vehicle
    private client: Client
    private rentalInformation: RentalInformation

    constructor(vehicle: Vehicle, client: Client, rentalInformation: RentalInformation) {
        this.vehicle = vehicle
        this.client = client
        this.rentalInformation = rentalInformation
    }

    calculateBillAndPrintReport(rentalPrice: VehicleRentalPrice, insurancePrice: VehicleInsurancePrice) {
        const rentalService = new VehicleRentalService()
        const rentalBill: RentalBill = rentalService.calculate(rentalPrice, insurancePrice, this.rentalInformation)
    
        const rentalReport = new VehicleRentalReport(this.vehicle, this.client, rentalBill)
        rentalReport.print()
    }
}
