import Client from "../../domain/Client";
import Motorbike from "../../domain/Motorbike";
import { VehicleInsurancePrice } from "../../domain/interfaces/VehicleInsurancePrice";

export class MotorbikeInsurancePrice implements VehicleInsurancePrice {

    private motorbike: Motorbike;
    private client: Client;

    constructor(motorbike: Motorbike, client: Client) {
        this.motorbike = motorbike;
        this.client = client;
    }

    calculate(): number {
        let insurancePrice = 30;
        const currentYear = new Date().getFullYear();
        const ageInYears = currentYear - this.motorbike.date.getFullYear();

        if (this.motorbike.color === 'black') {
            insurancePrice += 10;
        }

        if (this.motorbike.model === 'Honda') {
            insurancePrice += 20;
        }

        if (ageInYears <= 2) {
            insurancePrice += 20;
        }

        if (this.client.age < 20) {
            insurancePrice += 25;
        }

        return insurancePrice;
    }

}
