import { VehicleInsurancePrice } from "../../domain/interfaces/VehicleInsurancePrice"
import { RentalBill } from "../../domain/RentalBill"
import { RentalInformation } from "../../domain/RentalInformation"
import { VehicleRentalPrice } from "../../domain/interfaces/VehicleRentalPrice"

export class VehicleRentalService {

    calculate(vehicleRentalPrice: VehicleRentalPrice, vehicleInsurancePrice: VehicleInsurancePrice, rentalInformation: RentalInformation): RentalBill {
        const rentalBill = new RentalBill()

        rentalBill.priceByDay = vehicleRentalPrice.calculate()
        rentalBill.insurancePrice = vehicleInsurancePrice.calculate()
        rentalBill.rentalDays = rentalInformation.rentalDays
        rentalBill.total = (rentalBill.rentalDays * rentalBill.priceByDay) + rentalBill.insurancePrice

        return rentalBill
    }

}