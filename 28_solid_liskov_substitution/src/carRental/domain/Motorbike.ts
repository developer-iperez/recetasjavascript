import Vehicle from "./Vehicle"

export default class Motorbike extends Vehicle {

    private _includesHelmet: boolean

    constructor() {
        super('Motorbike')
        this._includesHelmet = false
    }

    public get includeHelmet() {
        return this._includesHelmet
    }

    public set includeHelmet(includesHelmet: boolean) {
        this._includesHelmet = includesHelmet
    }

    getExtras(): Array<string> {
        return ['Helmet']
    }
}