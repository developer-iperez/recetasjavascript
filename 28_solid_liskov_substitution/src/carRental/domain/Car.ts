import Vehicle from "./Vehicle"

export default class Car extends Vehicle {

    private _includesChildSeats: boolean

    constructor() {
        super('Car')
        this._includesChildSeats = false
    }

    public get includesChildSeats(): boolean {
        return this._includesChildSeats
    }

    public set includesChildSeats(includesChildSeats: boolean) {
        this._includesChildSeats = includesChildSeats
    }

    getExtras(): Array<string> {
        return ['Child seats']
    }

}