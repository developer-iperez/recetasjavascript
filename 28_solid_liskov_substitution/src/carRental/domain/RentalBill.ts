export class RentalBill {

    private _rentalDays: number
    private _priceByDay: number
    private _insurancePrice: number
    private _total: number

    constructor() {
        this._rentalDays = 0
        this._priceByDay = 0
        this._insurancePrice = 0
        this._total = 0
    }

    get rentalDays() {
        return this._rentalDays
    }

    set rentalDays(rentalDays: number) {
        this._rentalDays = rentalDays
    }

    get priceByDay() {
        return this._priceByDay
    }

    set priceByDay(priceByDay: number) {
        this._priceByDay = priceByDay
    }

    get insurancePrice() {
        return this._insurancePrice
    }

    set insurancePrice(insurancePrice: number) {
        this._insurancePrice = insurancePrice
    }

    get total() {
        return this._total
    }

    set total(total: number) {
        this._total = total
    }

}