// https://www.samanthaming.com/tidbits/70-3-ways-to-clone-objects/
// https://www.codementor.io/@junedlanja/copy-javascript-object-right-way-ohppc777d
// https://stackoverflow.com/questions/32925460/object-spread-vs-object-assign
// https://stackoverflow.com/questions/28150967/typescript-cloning-object

function getTimePrecission() {
    // https://stackoverflow.com/questions/11725691/how-to-get-a-microtime-in-node-js
    var hrTime = process.hrtime()       // [0] -> seconds, [1] -> nanoseconds
    //console.log(hrTime[0] * 1000000 + hrTime[1] / 1000)
    //hrTime = hrTime[0] * 1000000 + hrTime[1] / 1000

    // [0]+1000 -> millis, [1]/100000 -> millis
    hrTime =  (hrTime[0] * 1000) + (hrTime[1] / 1000000)
    return hrTime
}

function getUser() {
    return new User('john', 'pa22word', ['guest', 'editor'], 
        new Address('Spain', 'Barcelona', 'Pl. Sant Jaume 1', '08002'))
}

class Address {
    constructor(country, city, street, zip) {
        this.country = country
        this.city = city
        this.street = street
        this.zip = zip
    }
}

class User {
    constructor(name, password, roles, address) {
        this.name = name
        this.password = password
        this.roles = roles
        this.address = address
    }
}

class UserDataBase {
    constructor() {
        this.users = []
    }

    add(user) {
        this.users.push(user)
    }
}

const db = new UserDataBase()
for (let i = 0; i < 100000; i++)
    db.add(getUser())

/////////////////////
// Test Performance

console.log(`\n`)
console.log(`--------------------`)
console.log(`- Performance test -`)
console.log(`\n`)

// Spread 
const spreadTime1 = getTimePrecission()
const userCopySpread = { ...db}
const spreadTime2 = getTimePrecission()
console.log(`[Spread] Clone object with Spread... time elapsed: ${spreadTime2 - spreadTime1}`)

// Object.assign
const objectAssignTime1 = getTimePrecission()
const userCopyObjectAssign = Object.assign({}, db)
const objectAssignTime2 = getTimePrecission()
console.log(`[Object.assign] Clone object with Object.assign... time elapsed: ${objectAssignTime2 - objectAssignTime1}`)

// JSON
const jsonTime1 = getTimePrecission()
const userCopyJson = JSON.parse(JSON.stringify(db))
const jsonTime2 = getTimePrecission()
console.log(`[JSON.parse/JSON.stringify] Clone object with JSON methods... time elapsed: ${jsonTime2 - jsonTime1}`)

/////////////////////
// Test Clone

console.log(`\n`)
console.log(`--------------`)
console.log(`- Clone test -`)
console.log(`\n`)

// Test Spread
let originalUser = getUser()
const userClonedSpread = { ...originalUser }

userClonedSpread.name = 'other_name'
userClonedSpread.address.country = 'other_country'
console.log('[Spread] User names must be different: ' + (originalUser.name !== userClonedSpread.name))
console.log('[Spread] Country value is the same (shallow copy): ' + (originalUser.address.country === userClonedSpread.address.country))

// Test Object.assign
originalUser = getUser()
const userClonedAssign = Object.assign({}, originalUser)

userClonedAssign.name = 'other_name'
userClonedAssign.address.country = 'other_country'
console.log('[Object.assign] User names must be different: ' + (originalUser.name !== userClonedAssign.name))
console.log('[Object.assign] Country value is the same (shallow copy): ' + (originalUser.address.country === userClonedAssign.address.country))

// Test JSON.parse / JSON.stringify
originalUser = getUser()
const userClonedJSON = JSON.parse(JSON.stringify(originalUser))

userClonedJSON.name = 'other_name'
userClonedJSON.address.country = 'other_country'
console.log('[JSON.parse/JSON.stringify] User names must be different: ' + (originalUser.name !== userClonedJSON.name))
console.log('[JSON.parse/JSON.stringify] Country value must be different (deep copy): ' + (originalUser.address.country !== userClonedJSON.address.country))
