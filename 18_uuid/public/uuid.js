// https://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid/2117523#2117523
function uuidMathBased() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

// https://gist.github.com/outbreak/316637cde245160c2579898b21837c1c
function uuidWithCrypto () {
    function getRandomSymbol (symbol) {
        var array;

        if (symbol === 'y') {
            array = ['8', '9', 'a', 'b'];
            return array[Math.floor(Math.random() * array.length)];
        }

        array = new Uint8Array(1);
        window.crypto.getRandomValues(array);
        return (array[0] % 16).toString(16);
    }
    
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, getRandomSymbol);
}

// https://medium.com/teads-engineering/generating-uuids-at-scale-on-the-web-2877f529d2a2
function uuidWithcreateObjectURL() {
    const url = URL.createObjectURL(new Blob());
    const uuid = url.substring(url.lastIndexOf('/') + 1);
    URL.revokeObjectURL(url);
    return uuid;
}