// npmjs.com/package/uuid
import { v4 as uuidv4 } from 'uuid';
import express from 'express';
import path from 'path';

const app = express();
app.use('/js', express.static(path.join(path.resolve(), 'public')));
app.use('/', (request, response) => {
    response.send(`
    <!doctype html>
    <html lang="en">
        <head>
        <meta charset="utf-8">
        <title>UUID use examples</title>
        <meta name="description" content="The HTML5 Herald">
        <meta name="author" content="SitePoint">
    </head>

    <body>
        <h3>Generating UUID with the NodeJS uuid module:</h3>
        ${uuidv4()}<br>
        ${uuidv4()}<br>

        <script src="js/uuid.js"></script>
        <script src="js/uuidExamples.js"></script>
    </body>
    </html>
    `);
});

app.listen(3000);
