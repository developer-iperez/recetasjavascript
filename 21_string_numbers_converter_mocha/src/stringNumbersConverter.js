/*
https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Number

Number.isNaN
Number.isInteger
Number.parseInt
Number.parseFloat
Number.EPSILON

typeof number !== 'number'
Math.pow
Math.ceil
Math.floor
toString
toFixed
*/

class StringNumberConverter {

    toNumber(string) {
        if (typeof string !== 'string')         // Detects null, [], false, true, '', ...   
            return NaN;
        if(Number.isNaN(string))
            return NaN;
        if(string.match(/[a-zA-Z]+/i) !== null)
            return NaN;
        if(Number.isInteger(string))
            return Number.parseInt(string);
        return Number.parseFloat(string);
    }

    toString(number, decimals, roundDecimals = true) {
        if (typeof number !== 'number')         // Detects null, [], false, true, '', ...   
            return NaN;
        if (isNaN(number))                      // Detects the NaN case
            return NaN;

        const decimalsParsed = this.toNumber(String(decimals));
        if (isNaN(decimalsParsed))    
            return number.toString();

        roundDecimals = !!roundDecimals;        // Getting the equivalent boolean value
        
        if (roundDecimals === false && decimals >= 0) {
            const roundingDecimals = parseInt(Math.pow(10, decimals));
            const partialResult = (number + Number.EPSILON) * roundingDecimals;
            const result = (partialResult < 0) ? Math.ceil(partialResult) : Math.floor(partialResult);
            return String(result / roundingDecimals);
        }

        return number.toFixed(decimalsParsed);
    }

}

export default StringNumberConverter;
