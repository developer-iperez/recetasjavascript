import assert from 'assert';
import StringNumberConverter from '../src/stringNumbersConverter.js';

describe('StringNumbersConverter test suite', function() {

    context('String to number tests' , function() {

        beforeEach(function() {
            this.converter = new StringNumberConverter();
        })

        it('null|undefined|[]|false|empty string -> NaN', function() {
            assert.strictEqual(NaN, this.converter.toNumber(null))
            assert.strictEqual(NaN, this.converter.toNumber(undefined))
            assert.strictEqual(NaN, this.converter.toNumber([]))
            assert.strictEqual(NaN, this.converter.toNumber(false))
            assert.strictEqual(NaN, this.converter.toNumber(''))
        })

        it('invalid number string -> NaN', function() {
            assert.strictEqual(NaN, this.converter.toNumber('F'))
            assert.strictEqual(NaN, this.converter.toNumber('F*CK THE JS'))
            assert.strictEqual(NaN, this.converter.toNumber('+-+-+-+-0'))
            assert.strictEqual(NaN, this.converter.toNumber('1.aaaa232wdddd'))      // This string pass the Number.NaN() as a non Nan value!
        })

        it('negative string integer -> negative integer', function() {
            assert.strictEqual(-1, this.converter.toNumber('-1'));
            assert.strictEqual(-Math.pow(10, 10), this.converter.toNumber('-10000000000'));
        })

        it('negative string float -> negative float', function() {
            assert.strictEqual(-1.23, this.converter.toNumber('-1.23'));
            assert.strictEqual(-(Math.pow(10, 10) + 0.23), this.converter.toNumber('-10000000000.23'));
        })
    
        it('positive string integer -> positive integer', function() {
            assert.strictEqual(1, this.converter.toNumber('1'));
            assert.strictEqual(Math.pow(10, 10), this.converter.toNumber('10000000000'));
        })

        it('positive string float -> positive float', function() {
            assert.strictEqual(1.23, this.converter.toNumber('1.23'));
            assert.strictEqual(Math.pow(10, 10) + 0.23, this.converter.toNumber('10000000000.23'));
        })
    
        it('zero string number -> zero', function() {
            assert.strictEqual(0, this.converter.toNumber('0'));
            assert.strictEqual(0, this.converter.toNumber('0.0'));
        })
    
        it('multiple zero string -> zero', function() {
            assert.strictEqual(0, this.converter.toNumber('0000000000000000000'));
        })
    })

    context('Number to string tests' , function() {

        beforeEach(function() {
            this.converter = new StringNumberConverter();
        })
        
        it('null|undefined|[]|false|true|NaN|empty string -> NaN', function() {
            assert.strictEqual(NaN, this.converter.toString(null))
            assert.strictEqual(NaN, this.converter.toString(undefined))
            assert.strictEqual(NaN, this.converter.toString([]))
            assert.strictEqual(NaN, this.converter.toString(false))
            assert.strictEqual(NaN, this.converter.toString(true))
            assert.strictEqual(NaN, this.converter.toString(NaN))
            assert.strictEqual(NaN, this.converter.toString(''))
        })

        it('invalid number -> NaN', function() {
            assert.strictEqual(NaN, this.converter.toString('F'))
            assert.strictEqual(NaN, this.converter.toString('F*CK THE JS'))
            assert.strictEqual(NaN, this.converter.toString('+-+-+-+-0'))
            assert.strictEqual(NaN, this.converter.toString('1.aaaa232wdddd'))
        })

        it('Special numbers -> Equivalent string', function() {
            assert.strictEqual('1.7976931348623157e+308', this.converter.toString(Number.MAX_VALUE))
            assert.strictEqual('5e-324', this.converter.toString(Number.MIN_VALUE))
            assert.strictEqual('Infinity', this.converter.toString(Number.POSITIVE_INFINITY))
            assert.strictEqual('-Infinity', this.converter.toString(Number.NEGATIVE_INFINITY))
        })

        it('Integer string -> Integer', function() {
            assert.strictEqual('-10000000000', this.converter.toString(-Math.pow(10, 10)))
            assert.strictEqual('-1', this.converter.toString(-1))
            assert.strictEqual('0', this.converter.toString(0))
            assert.strictEqual('1', this.converter.toString(1))
            assert.strictEqual('10000000000', this.converter.toString(Math.pow(10, 10)))
        })

        it('Float string -> Float', function() {
            assert.strictEqual('-10000000000.23', this.converter.toString(-(Math.pow(10, 10) + 0.23)))
            assert.strictEqual('-1.23', this.converter.toString(-1.23))
            assert.strictEqual('0', this.converter.toString(0.0))
            assert.strictEqual('1.23', this.converter.toString(1.23))
            assert.strictEqual('10000000000.23', this.converter.toString(Math.pow(10, 10) + 0.23))
        })

        it('Float string with concrete decimals (rounding) -> Float', function() {
            assert.strictEqual('-2', this.converter.toString(-1.9365678, 0))
            assert.strictEqual('-1.24', this.converter.toString(-1.2365678, 2))
            assert.strictEqual('0.0000', this.converter.toString(0, 4))
            assert.strictEqual('1.24', this.converter.toString(1.2365678, 2))
            assert.strictEqual('2', this.converter.toString(1.9365678, 0))
        })

        it('Float string with concrete decimals (no rounding) -> Float', function() {
            assert.strictEqual('-1000.2345678', this.converter.toString(-1000.23456789, 7, false))
            assert.strictEqual('-1', this.converter.toString(-1.9365678, 0, false))
            assert.strictEqual('-1.23', this.converter.toString(-1.2395678, 2, false))
            assert.strictEqual('0', this.converter.toString(0.0, 2, false))
            assert.strictEqual('1.23', this.converter.toString(1.2395678, 2, false))
            assert.strictEqual('1', this.converter.toString(1.9365678, 0, false))
            assert.strictEqual('1000.2345678', this.converter.toString(1000.23456789, 7, false))
        })
    })

})
