// Implementamos una función
function simpleFunction() {
    console.log("Hello, I'm a simple function");
}

// Invocamos la función
simpleFunction();  // Imprime "Hello, I'm a simple function"

////////////////////////////////////////////////

// Implementamos una función con parámetros y con valor de retorno
function functionWithParameters(parameter1, parameter2) {
    console.log("Hello " + parameter1 + " " + parameter2);
    return parameter1 + parameter2;
}

// Invocamos la función
const string = functionWithParameters("Hari", "Seldon");  // Imprime "Hello Hari Seldon"
console.log(string);  // Imprime "HariSeldon"

////////////////////////////////////////////////

function buyApples(count) {
    count++;
    console.log("Buy apples, total: " + count);
    return count;
}

function eatApples(count) {
    count--;
    console.log("Eat apples, total: " + count);
    return count;
}

let count = 0;
count = buyApples(count);
count = buyApples(count);
count = eatApples(count);
count = buyApples(count);
count = buyApples(count);
count = eatApples(count);
console.log("Apples: " + count);

////////////////////////////////////////////////

let f = function() {
    console.log("Hello, I'm a function too")
}
f();  // Muestra en la consola "Hello, I'm a function too"

////////////////////////////////////////////////

var f2 = () => {
    console.log("Hello, I'm an arrow function")
}
f2();  // Muestra en la consola "Hello, I'm an arrow function"
