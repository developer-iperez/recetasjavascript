let a = 100;
let b = 200;
let c = a + b;
console.log(c);    // 300

////////////////////////////////////////////////

console.log('apple' == 'pear');   // false
console.log(12 == 24);            // false
console.log(1 == true);           // true
console.log(0 == false);          // true
console.log(0 == '');             // true
console.log(0 == '0');            // true

////////////////////////////////////////////////

console.log('apple' === 'pear');  // false
console.log(12 === 24);           // false
console.log(1 === true);          // false
console.log(0 === false);         // false
console.log(0 === '');            // false
console.log(0 === '0');           // false

////////////////////////////////////////////////

let result = 100 + 50;
console.log(result);     // 150
result += 50;
console.log(result)      // 200

////////////////////////////////////////////////

let value = 100;
console.log(value != 101);      // true
console.log(value !== '100');   // true
console.log(value !== 100);     // false
console.log(value > 100);       // false
console.log(value >= 100);      // true
console.log(value < 100);       // false
console.log(value <= 100);      // true
