let valueInteger = 100;          // Número entero
let valueHexadecimal = 0xFF;     // Número hexadecimal... empieza por 0x...
let valueOctal = 0400;           // Número octal... empieza por 0...
let valueDecimal = 3.141592;     // ¡Mi número favorito!
let valueDecimal2 = 1.15e10;     // Equivale a 1.15 x 10 elevado a la 10
let valueSuma = 10 + 20;
let valueResta = 20 - 10;
let valueMultiplica = 10 + 2;
let valueDivide = 10 / 2;

console.log('valueInteger ' + valueInteger);
console.log('valueHexadecimal ' + valueHexadecimal);
console.log('valueOctal ' + valueOctal);
console.log('valueDecimal ' + valueDecimal);
console.log('valueDecimal2 ' + valueDecimal2);
console.log('valueSuma ' + valueSuma);
console.log('valueResta ' + valueResta);
console.log('valueMultiplica ' + valueMultiplica);
console.log('valueDivide ' + valueDivide);

const text1 = "Texto de ejemplo";         // No puedo usar comillas dobles en el texto...
const text2 = "Texto de \"ejemplo\"";     // ... bueno, sí, si "escapo" las comillas con "\"
const text3 = 'Otro texto de "ejemplo"';  // Aquí puedo usar comillas dobles en el texto!

console.log('text1 ' + text1);
console.log('text2 ' + text2);
console.log('text2 ' + text2);

let valueTrue = true;
let valueFalse = false;
let valueAnd = valueTrue && valueFalse;  // Operación lógica AND, el resultado es false
let valueOr = valueTrue || valueFalse;  // Operación lógica OR, el resultado es true

console.log('valueTrue ' + valueTrue);
console.log('valueFalse ' + valueFalse);
console.log('valueAnd ' + valueAnd);
console.log('valueOr ' + valueOr);
