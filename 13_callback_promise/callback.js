function getTime() {
    const now = new Date();
    let date =  `${('0' + now.getHours()).slice(-2)}`;
    date += `:`;
    date += `${('0' + now.getMinutes()).slice(-2)}`;
    date += `:`;
    date += `${('0' + now.getSeconds()).slice(-2)}`;
    return date;
}

function isValidCallback(callback) {
    return (callback !== undefined && callback !== null && typeof(callback) === 'function');
}

function runCallbackSynchronously(callback) {
    if (isValidCallback(callback) === false) {
        console.log('ERROR! Invalid callback');
    }

    callback.call();
}

function runCallbackAsynchronously(callback, secondsToWait) {
    if (isValidCallback(callback) === false) {
        console.log('ERROR! Invalid callback');
    }

    setTimeout(() => {
        callback.call();
    }, secondsToWait * 1000);
}

console.log(`[${getTime()}] Starting callback example`);

console.log(`[${getTime()}] runCallbackSynchronously`);
runCallbackSynchronously(() => {
    console.log('Hello from callback, sync...');
});

console.log(`[${getTime()}] runCallbackAsynchronously`);
runCallbackAsynchronously(() => {
    // Muestra este mensaje pasados 2 segundos
    console.log('Hello from callback, async...');
}, 2);
