let database = [
    {
        id: 1,
        mails: []
    }
];

let mailingList = {
    getMailingListAsynchronous: (id) => {
        return new Promise((resolve, reject) => {
            let mailingList = database.find((o) => {
                return o.id === id
            });

            if (mailingList) {
                resolve(mailingList);
            }
            else {
                reject(`Mailing list not found with the ID ${id}`);
            }
        }); 
    },
    
    addMailToMailingListAsynchronous: (mailingList, mail) => {
        return new Promise((resolve, reject) => {
            let existingMailingList = database.find((o) => {
                return o.id === mailingList.id
            });
        
            if (existingMailingList) {
                mailingList.mails.push(mail);
                resolve(mailingList);
            }
            else {
                reject(`Mailing List not found with the ID ${mailingList.id}`);
            }
        }); 
    },
    
    validateMailAsynchronous: (mail) => {
        return new Promise((resolve, reject) => {
            if (mail === null || mail === undefined || mail === '') {
                reject('Mail bad format');
            } else {
                resolve();
            }
        }); 
    },
    
    notificateAsynchronous: (mailingList, successCallback, errorCallback) => {
        return new Promise((resolve, reject) => {
            for (let mail of mailingList.mails) {
                console.log(`Notification sended to mail: "${mail}"`);
            }
        
            resolve();
        });
    }
};

module.exports = mailingList;
