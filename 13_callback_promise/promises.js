function runCallbackAsynchronously(secondsToWait) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve();
        }, secondsToWait * 1000);
    });
}

runCallbackAsynchronously(2)
    .then(() => {
        // Muestra este mensaje pasados 2 segundos
        console.log('Hello from promise, async...');
    })
    .catch((error) => {
        console.log(`ERROR! ${error}`);
    });
