let database = [
    {
        id: 1,
        mails: []
    }
];

let mailingList = {
    getMailingListAsynchronous: (id, successCallback, errorCallback) => {
        let user = database.find((o) => {
            return o.id === id
        });
    
        if (user) {
            successCallback(user);
        }
        else {
            errorCallback(`User not found with the ID ${userId}`);
        }
    },
    
    addMailToMailingListAsynchronous: (mailingList, mail, successCallback, errorCallback) => {
        let existingMailingList = database.find((o) => {
            return o.id === mailingList.id
        });
    
        if (existingMailingList) {
            mailingList.mails.push(mail);
            successCallback(mailingList);
        }
        else {
            errorCallback(`Mailing List not found with the ID ${mailingList.id}`);
        }
    },
    
    validateMailAsynchronous: (mail, successCallback, errorCallback) => {
        if (mail === null || mail === undefined || mail === '') {
            errorCallback('User mail bad format');
        } else {
            successCallback();
        }
    },
    
    notificateAsynchronous: (mailingList, successCallback, errorCallback) => {
        for (let mail of mailingList.mails) {
            console.log(`Notification sended to mail: "${mail}"`);
        }
    
        successCallback();
    }
};

module.exports = mailingList;
