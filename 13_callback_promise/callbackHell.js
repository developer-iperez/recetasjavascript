let mailingListModule = require('./mailingList_callback');

function addMailToMailingList(id, mail) {
    // Validamos el mail
    mailingListModule.validateMailAsynchronous(mail, () => {
        // Obtenemos la lista de correo desde la fuente de datos
        mailingListModule.getMailingListAsynchronous(id, (mailingList) => {
            // Añadimos el nuevo mail a la lista de correo
            mailingListModule.addMailToMailingListAsynchronous(mailingList, mail, () => {
                // Notificamos al mail añadido que ha sido añadido a la lista de correo
                mailingListModule.notificateAsynchronous(mailingList, () => {
                    console.log('Update mail list: succeded');
                }, (error) => {
                    console.log(`Error on send notification: "${error}"`);
                });    
            }, (error) => {
                console.log(`Error on update mailing list information: "${error}"`);
            });
        }, (error) => {
            console.log(`Error on retrieve mailing list: "${error}"`);
        });
    }, (error) => {
        console.log(`Error on validate mail: "${error}"`);
    });
}

addMailToMailingList(1, 'user1@mail.com');
