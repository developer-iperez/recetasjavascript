let mailingListModule = require('./mailingList_promises');

function addMailToMailingList(id, mail) {
    // Validamos el mail
    mailingListModule.validateMailAsynchronous(mail)
        .then(() => {
            // Obtenemos la lista de correo desde la fuente de datos
            return mailingListModule.getMailingListAsynchronous(id);
        })
        .then((mailingList) => {
            // Añadimos el nuevo mail a la lista de correo
            return mailingListModule.addMailToMailingListAsynchronous(mailingList, mail);
        })
        .then((mailingList) => {
            // Notificamos al mail añadido que ha sido añadido a la lista de correo
            return mailingListModule.notificateAsynchronous(mailingList);
        })
        .then(() => {
            console.log('Update mail list: succeded');
        })
        .catch((error) => {
            console.log(`Error on send notification: "${error}"`);
        });
}

addMailToMailingList(1, '');
addMailToMailingList(1, 'user1@mail.com');
