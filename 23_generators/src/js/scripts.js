class CurrencyDataRequest {
    constructor(currency) {
        this.currency = currency
    }

    async request() {
        const api = `https://api.coincap.io/v2/assets/${this.currency}`
        const response = await fetch(api)
        const result = await response.json()
        let price = null

        if (result.data) {
            price = Number.parseFloat(result.data.priceUsd)
        }

        return price
    }
}

class CurrencyRenderer {
    constructor(elementId, options) {
        this.elementId = elementId
        this.options = options || {}
    }

    render(value, differential) {
        const element = document.getElementById(this.elementId)
        const price = value.toFixed(this.options.priceDecimals || 0)

        element.textContent = `${this.options.priceSymbol || ''}${price}`

        element.classList.remove('diff-positive', 'diff-negative')
        if (differential >= 0) {
            element.classList.add('diff-positive')
        } else {
            element.classList.add('diff-negative')
        }
    }
}

class CurrencyRendererOptions {
    constructor() {
        this.priceSymbol = 'N/A'
        this.priceDecimals = 0
    }
}

class CryptoCurrencyPriceItem {
    constructor(request, renderer) {
        this.request = request
        this.renderer = renderer
        this.lastResult = NaN
    }

    async requestAndRender() {
        const result = await this.request.request()
        const differential = isNaN(this.lastResult) ? 0 : this.lastResult - result
        
        this.renderer.render(result, differential)
        this.lastResult = result
    }
}

class Application {
    constructor(currencyCollection, secondsToRequestNewValue) {
        this.intervalId = null
        this.intervalMilliseconds = secondsToRequestNewValue * 1000
        this.currencies = currencyCollection || []
        this._generator = this._requestAndRenderCurrency()
    }
    
    async start() {
        if (this.intervalId === null) {
            this._generator.next()

            this.intervalId = setInterval(async () => {
                this._generator.next()
            }, this.intervalMilliseconds)
        }
    }

    async *_requestAndRenderCurrency() {
        let i = 0

        while(true) {
            yield await this.currencies[i % this.currencies.length].requestAndRender()
            i++
        }
    }
}

(async () => {
    const currencies = [new CryptoCurrencyPriceItem(
        new CurrencyDataRequest('bitcoin'), 
        new CurrencyRenderer('bitcoin', { 
            priceDecimals: 2,
            priceSymbol: '$'  
        })
    ), new CryptoCurrencyPriceItem(
        new CurrencyDataRequest('ethereum'), 
        new CurrencyRenderer('ethereum', { 
            priceDecimals: 3,
            priceSymbol: '$'  
        })
    )]

    const app = new Application(currencies, 3)
    await app.start()
})()
