let database = [
    {
        id: 1,
        mails: []
    }
];

let mailingList = {
    getMailingListAsynchronous: async (id) => {
        let mailingList = database.find((o) => {
            return o.id === id
        });

        if (mailingList) {
            return mailingList;
        }
        else {
            throw new Error(`Mailing list not found with the ID ${id}`);
        }
    },
    
    addMailToMailingListAsynchronous: async (mailingList, mail) => {
        let existingMailingList = database.find((o) => {
            return o.id === mailingList.id
        });
    
        if (existingMailingList) {
            mailingList.mails.push(mail);
            return mailingList;
        }
        else {
            throw new Error(`Mailing List not found with the ID ${mailingList.id}`);
        }
    },
    
    validateMailAsynchronous: async (mail) => {
        if (mail === null || mail === undefined || mail === '') {
            throw new Error('Mail bad format');
        }
    },
    
    notificateAsynchronous: async (mailingList, successCallback, errorCallback) => {
        for (let mail of mailingList.mails) {
            console.log(`Notification sended to mail: "${mail}"`);
        }
    }
};

module.exports = mailingList;
