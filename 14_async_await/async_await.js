let mailingListModule = require('./mailingList_async_await');

async function addMailToMailingList(id, mail) {
    let mailingList;
    
    try {
        // Validamos el mail
        await mailingListModule.validateMailAsynchronous(mail);
        // Obtenemos la lista de correo desde la fuente de datos
        mailingList = await mailingListModule.getMailingListAsynchronous(id);
        // Añadimos el nuevo mail a la lista de correo
        mailingList = await mailingListModule.addMailToMailingListAsynchronous(mailingList, mail);
        // Notificamos al mail añadido que ha sido añadido a la lista de correo
        mailingList = await mailingListModule.notificateAsynchronous(mailingList);

        console.log('Update mail list: succeded');
    }
    catch (error) {
        console.log(`Error on send notification: "${error}"`);
    }
}

addMailToMailingList(1, '');
addMailToMailingList(1, 'user1@mail.com');
