function getCustomTimeFormat() {
    const now = new Date();
    let date =  `${('0' + now.getHours()).slice(-2)}`;
    date += `:`;
    date += `${('0' + now.getMinutes()).slice(-2)}`;
    date += `:`;
    date += `${('0' + now.getSeconds()).slice(-2)}`;
    return date;
}

const now = new Date();
console.log(`Current date and time...`);
// Output: 17
console.log(`getDate: ${now.getDate()}`);
// Output: 5
console.log(`getMonth: ${now.getMonth() + 1}`);
// Output: 2020
console.log(`getFullYear: ${now.getFullYear()}`);
// Output: 10
console.log(`getHours: ${now.getHours()}`);
// Output: 36
console.log(`getMinutes: ${now.getMinutes()}`);
// Output: 14
console.log(`getSeconds: ${now.getSeconds()}`);
// Output: 288
console.log(`getMilliseconds: ${now.getMilliseconds()}`);

console.log(`\nCurrent date and time to string...`);
// Output: Sun May 17 2020 10:36:14 GMT+0200 (GMT+02:00)
console.log(`toString: ${now.toString()}`);
// Output: Sun May 17 2020
console.log(`toDateString: ${now.toDateString()}`);
// Output: 10:36:14 GMT+0200 (GMT+02:00)
console.log(`toTimeString: ${now.toTimeString()}`);
// Output: 2020-5-17 10:36:14
console.log(`toLocaleString: ${now.toLocaleString()}`);
// Output: 2020-5-17
console.log(`toLocaleDateString: ${now.toLocaleDateString()}`);
// Output: 10:36:14
console.log(`toLocaleTimeString: ${now.toLocaleTimeString()}`);
// Output: 2020-05-17T08:36:14.288Z
console.log(`toISOString: ${now.toISOString()}`);
// Output: Sun, 17 May 2020 08:36:14 GMT
console.log(`toUTCString: ${now.toUTCString()}`);

console.log(`\nCurrent date and time to custom string...`);
// Output: 10:36:14
console.log(`Custom Time Format: ${getCustomTimeFormat()}`);

const locale = 'es-ES';
const timezoneUTC = { timeZone: 'UTC' };
const timezoneMadrid = { timeZone: 'Europe/Madrid' };
const timezoneNewYork = { timeZone: 'America/New_York' };
const timezoneTokyo = { timeZone: 'Asia/Tokyo' };
const date = new Date(Date.UTC(2020, 0, 1, 10, 0, 0));
console.log(`\nExample with different timezones...`);
// Output: UTC: 2020-1-1 10:00:00
console.log(`UTC -> ${date.toLocaleString(locale, timezoneUTC)}`);
// Output: Madrid: 2020-1-1 11:00:00
console.log(`Madrid -> ${date.toLocaleString(locale, timezoneMadrid)}`);
// Output: New York: 2020-1-1 5:00:00
console.log(`New York ->  ${date.toLocaleString(locale, timezoneNewYork)}`);
// Output: Tokyo: 2020-1-1 19:00:00
console.log(`Tokyo -> ${date.toLocaleString(locale, timezoneTokyo)}`);
