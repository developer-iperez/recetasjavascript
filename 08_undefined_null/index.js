let variable;

// Ouput: "undefined"
console.log(variable);

// Output: "The variable has no value assigned"
if (variable === undefined) {
    console.log('The variable has no value assigned');
}

function myfunction() {
    console.log('This function has not return value');
}

// Output: "This function has not return value"
// Output: "undefined"
console.log(myfunction());

// Output: "The variable has no value"
variable = null;
if (variable === null) {
    console.log('The variable has no value');
}

// Output: true
console.log(null == undefined);
// Output: false
console.log(null === undefined);
