function addItemToShoppingCart(shoppingCart, item) {
    shoppingCart.push(item);
}

function removeLastItemFromShoppingCart(shoppingCart) {
    shoppingCart.pop();
}

function clearShoppingCart(shoppingCart) {
    shoppingCart.length = 0;
}

function printShoppingCart(shoppingCart) {
    console.log(shoppingCart.toString());
}

function processOrder(date, shoppingCart, shoppingHistory) {
    let order = [];
    let cartCopy = shoppingCart.slice();
    order.push(date);
    order = order.concat(cartCopy);
    shoppingHistory.push(order);
}

function printShoppingHitory(shoppingHistory) {
    console.log(shoppingHistory.toString());
}

let shoppingCart = [];                              // Temporal cart shopping list
let shoppingHistory = [];                           // Shopping history list

addItemToShoppingCart(shoppingCart, 'watermelon');
addItemToShoppingCart(shoppingCart, 'papaya');
addItemToShoppingCart(shoppingCart, 'peach');
removeLastItemFromShoppingCart(shoppingCart);

console.log("You shopping cart contains: ");
printShoppingCart(shoppingCart);                    // Print watermelon,papaya
console.log("Process order...");
processOrder('2030-12-06', shoppingCart, shoppingHistory);

clearShoppingCart(shoppingCart);
addItemToShoppingCart(shoppingCart, 'watermelon');
addItemToShoppingCart(shoppingCart, 'peach');
addItemToShoppingCart(shoppingCart, 'lemon');

console.log("You shopping cart contains: ");
printShoppingCart(shoppingCart);                    // Print watermelon,papaya
console.log("Process order...");
processOrder('2030-12-09', shoppingCart, shoppingHistory);

console.log("Your shopping shopping history is:");
// Next prints: "2030-12-06,watermelon,papaya,2030-12-09,watermelon,peach,lemon"
printShoppingHitory(shoppingHistory);               
