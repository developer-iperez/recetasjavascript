import Bike from "../../domain/Bike"
import { VehicleRentalPrice } from "../../domain/interfaces/VehicleRentalPrice"

export class BikeRentalPrice implements VehicleRentalPrice {

    private bike: Bike

    constructor(bike: Bike) {
        this.bike = bike
    }

    calculate(): number {
        const currentYear = new Date().getFullYear()
        const carAgeInYears = currentYear - this.bike.date.getFullYear()
        let price = 10

        if (carAgeInYears <= 1)
            price += 5

        if (this.bike.electric === true)
            price += 10

        return price
    }

}
