import Motorbike from "../../domain/Motorbike"
import { VehicleRentalPrice } from "../../domain/interfaces/VehicleRentalPrice"

export class MotorbikeRentalPrice implements VehicleRentalPrice {

    private motorbike: Motorbike

    constructor(motorbike: Motorbike) {
        this.motorbike = motorbike
    }

    calculate(): number {
        const currentYear = new Date().getFullYear()
        const carAgeInYears = currentYear - this.motorbike.date.getFullYear()
        let price = 0

        if (carAgeInYears <= 2) {
            price += 50
        }
        else if (carAgeInYears > 2 && carAgeInYears <= 5) {
            price += 40
        }
        else {
            price += 25
        }

        if (this.motorbike.includeHelmet === true)
            price += 5

        return price
    }

}
