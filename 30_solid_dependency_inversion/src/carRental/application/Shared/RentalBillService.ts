import { RentalBill } from "../../domain/RentalBill";
import { RentalInformation } from "../../domain/RentalInformation";
import { TotalRentalCalculator } from "./TotalRentalCalculator";

export class RentalBillService {

    private totalRentalCalculator: TotalRentalCalculator;

    constructor() {
        this.totalRentalCalculator = new TotalRentalCalculator();
    }

    public getRentalBill(rentalPrice: number, insurancePrice: number, rentalInformation: RentalInformation): RentalBill {
        const rentalBill = new RentalBill();

        rentalBill.priceByDay = rentalPrice;
        rentalBill.insurancePrice = insurancePrice;
        rentalBill.rentalDays = rentalInformation.rentalDays;
        rentalBill.total = this.totalRentalCalculator.calculate(rentalBill);

        return rentalBill;
    }
}