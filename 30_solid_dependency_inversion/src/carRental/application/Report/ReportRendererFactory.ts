import { VehicleRentalReportRenderer } from "../../domain/interfaces/VehicleRentalReportRenderer";
import { VehicleRentalReportHtml } from "../../infrastructure/Report/VehicleRentalReportHtml";
import { VehicleRentalReportMarkdown } from "../../infrastructure/Report/VehicleRentalReportMarkdown";
import { VehicleRentalReportSimple } from "../../infrastructure/Report/VehicleRentalReportSimple";
import { ReportType } from "../../domain/ReportType";

export class ReportRendererFactory {
    public static create(reportType: ReportType): VehicleRentalReportRenderer {
        if (reportType === ReportType.Html) {
            return new VehicleRentalReportHtml();
        } else if (reportType === ReportType.Markdown) {
            return new VehicleRentalReportMarkdown();
        } else {
            return new VehicleRentalReportSimple();
        }
    }
}
