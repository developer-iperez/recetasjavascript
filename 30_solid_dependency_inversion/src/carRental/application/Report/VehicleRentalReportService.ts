import Bike from "../../domain/Bike"
import Car from "../../domain/Car"
import Client from "../../domain/Client"
import Motorbike from "../../domain/Motorbike"
import { RentalBill } from "../../domain/RentalBill"
import { BikeReport } from "../../domain/interfaces/BikeReport"
import { CarReport } from "../../domain/interfaces/CarReport"
import { MotorbikeReport } from "../../domain/interfaces/MotorbikeReport"
import { VehicleRentalReportRenderer } from "../../domain/interfaces/VehicleRentalReportRenderer"

export class VehicleRentalReportService implements CarReport, MotorbikeReport, BikeReport {

    private vehicleRentalReportRender: VehicleRentalReportRenderer

    constructor(vehicleRentalReportRender: VehicleRentalReportRenderer) {
        this.vehicleRentalReportRender = vehicleRentalReportRender
    }

    printCarReport(car: Car, client: Client, rentalBill: RentalBill): void {
        this.vehicleRentalReportRender.print({
          vehicle: car,
          client,
          rentalBill,
          licenseNumber: car.licenseNumber  
        })
    }

    printMotorbikeReport(motorbike: Motorbike, client: Client, rentalBill: RentalBill): void {
        this.vehicleRentalReportRender.print({
            vehicle: motorbike,
            client,
            rentalBill,
            licenseNumber: motorbike.licenseNumber
        })
    }

    printBikeReport(bike: Bike, client: Client, rentalBill: RentalBill): void {
        this.vehicleRentalReportRender.print({
            vehicle: bike,
            client,
            rentalBill
        })
    }
}