import Car from "../../domain/Car"
import Client from "../../domain/Client"
import { VehicleInsurancePrice } from "../../domain/interfaces/VehicleInsurancePrice"

export class CarInsurancePrice implements VehicleInsurancePrice {

    private car: Car
    private client: Client

    constructor(car: Car, client: Client) {
        this.car = car
        this.client = client
    }

    calculate(): number {
        let insurancePrice = 50
        const currentYear = new Date().getFullYear()
        const carAgeInYears = currentYear - this.car.date.getFullYear()

        if (this.car.color === 'black') {
            insurancePrice += 50
        }
        else if (this.car.color === 'red' || this.car.color === 'yellow') {
            insurancePrice += 10
        }

        if (this.car.model === 'Mercedes') {
            insurancePrice += 50
        }

        if (carAgeInYears <= 1) {
            insurancePrice += 30
        }

        if (this.client.age < 25) {
            insurancePrice += 25
        }

        return insurancePrice
    }
    
}
