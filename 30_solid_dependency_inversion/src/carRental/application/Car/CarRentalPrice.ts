import Car from "../../domain/Car"
import { VehicleRentalPrice } from "../../domain/interfaces/VehicleRentalPrice"

export class CarRentalPrice implements VehicleRentalPrice {

    private car: Car

    constructor(car: Car) {
        this.car = car
    }

    calculate(): number {
        const currentYear = new Date().getFullYear()
        const carAgeInYears = currentYear - this.car.date.getFullYear()
        let price = 0

        if (carAgeInYears <= 1) {
            price += 100
        }
        else if (carAgeInYears > 1 && carAgeInYears <= 5) {
            price += 70
        }
        else {
            price += 40
        }
        
        if (this.car.includesChildSeats === true)
            price += 7

        return price
    }
}
