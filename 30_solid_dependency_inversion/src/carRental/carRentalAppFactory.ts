import { VehicleRentalReportService } from "./application/Report/VehicleRentalReportService";
import { VehicleRentalService } from "./application/Shared/VehicleRentalService";
import Car from "./domain/Car";
import { CarRentalService } from "./domain/interfaces/CarRentalService";
import { CarReport } from "./domain/interfaces/CarReport";
import Client from "./domain/Client";
import { RentalBill } from "./domain/RentalBill";
import { RentalInformation } from "./domain/RentalInformation";
import { ReportType } from "./domain/ReportType";
import { ReportRendererFactory } from "./application/Report/ReportRendererFactory";

class CarRentalAppFacade {

    private carRentalService: CarRentalService
    private carRentalReport: CarReport

    constructor(carRentalService: CarRentalService, carRentalReport: CarReport) {
        this.carRentalService = carRentalService
        this.carRentalReport = carRentalReport
    }

    calculateBill(car: Car, client: Client, rentalInformation: RentalInformation): RentalBill {
        const rentalBill: RentalBill = this.carRentalService.calculateCarRental(car, client, rentalInformation)
        return rentalBill
    }

    printReport(car: Car, client: Client, rentalBill: RentalBill) {
        this.carRentalReport.printCarReport(car, client, rentalBill)
    }
}

export class CarRentalAppFactory {

    create(reportType: ReportType): CarRentalAppFacade {
        const rentalReportRenderer = ReportRendererFactory.create(reportType)
        const carRentalService = new VehicleRentalService();
        const carRentalReportService = new VehicleRentalReportService(rentalReportRenderer);
        return new CarRentalAppFacade(carRentalService, carRentalReportService);
    }
}
