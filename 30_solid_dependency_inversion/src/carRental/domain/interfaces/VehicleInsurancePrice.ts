export interface VehicleInsurancePrice {
    calculate(): number
}
