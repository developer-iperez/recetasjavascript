import Bike from "../Bike"
import Client from "../Client"
import { RentalBill } from "../RentalBill"

export interface BikeReport {
    printBikeReport(bike: Bike, client: Client, rentalBill: RentalBill): void
}
