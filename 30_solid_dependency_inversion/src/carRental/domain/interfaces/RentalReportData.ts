import Client from "../Client";
import { RentalBill } from "../RentalBill";
import { Vehicle } from "../Vehicle";

export interface RentalReportData {
    vehicle: Vehicle
    client: Client
    rentalBill: RentalBill
    licenseNumber?: string
}
