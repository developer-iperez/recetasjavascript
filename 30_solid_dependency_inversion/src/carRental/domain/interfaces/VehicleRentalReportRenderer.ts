import { RentalReportData } from "./RentalReportData"

export interface VehicleRentalReportRenderer {
    print(rentalReportData: RentalReportData): void
}
