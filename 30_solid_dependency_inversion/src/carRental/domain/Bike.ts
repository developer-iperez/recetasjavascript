import { Vehicle } from "./Vehicle"

export default class Bike extends Vehicle {

    private _includesHelmet: boolean
    private _includesLocks: boolean
    private _electric: boolean

    constructor() {
        super('Bike')
        this._includesHelmet = true
        this._includesLocks = true
        this._electric = false
    }

    public get includeHelmet() {
        return this._includesHelmet
    }

    public set includeHelmet(includesHelmet: boolean) {
        this._includesHelmet = includesHelmet
    }

    public get includeLocks() {
        return this._includesLocks
    }

    public set includeLocks(includeLocks: boolean) {
        this._includesLocks = includeLocks
    }

    public get electric() {
        return this._electric
    }

    public set electric(electric: boolean) {
        this._electric = electric
    }

    getExtras(): Array<string> {
        let extras = []

        if (this.includeHelmet)
            extras.push('Helmet')
        if (this.includeLocks)
            extras.push('Locks')
        if (this.electric)
            extras.push('Electric bike')

        return extras
    }

}