export abstract class Vehicle {

    private _date: Date
    private _model: string
    private _color: string
    private _type: string

    constructor(type: string) {
        this._type = type
        this._date = new Date()
        this._model = ''
        this._color = ''
    }

    get type() {
        return this._type
    }
    
    get date() {
        return this._date
    }

    set date(date: Date) {
        this._date = date
    }

    get model() {
        return this._model
    }

    set model(model: string) {
        this._model = model
    }

    get color() {
        return this._color
    }

    set color(color: string) {
        this._color = color
    }

    abstract getExtras(): Array<string>
}