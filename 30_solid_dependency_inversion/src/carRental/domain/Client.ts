export default class Client {

    private _clientAge: number

    constructor() {
        this._clientAge = 20
    }

    get age() {
        return this._clientAge
    }

    set age(clientAge: number) {
        this._clientAge = clientAge
    }
}