
export enum ReportType {
    Simple = 'Simple',
    Html = 'Html',
    Markdown = 'Markdown'
}
