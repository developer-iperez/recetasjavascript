import { RentalReportData } from "../../domain/interfaces/RentalReportData"
import { VehicleRentalReportRenderer } from "../../domain/interfaces/VehicleRentalReportRenderer"

export class VehicleRentalReportHtml implements VehicleRentalReportRenderer {

    public print(rentalReportData: RentalReportData) {
        let report = ''

        report += `<html>
                        <head></head>
                        <body>
                            <h1>Rental vehicle report</h1>

                            <h2>Vehicle information</h2>
                            <ul>
                                <li>Type: ${rentalReportData.vehicle.type}</li>
                                <li>Model: ${rentalReportData.vehicle.model}</li>
                                <li>Date: ${rentalReportData.vehicle.date}</li>
                                <li>Color: ${rentalReportData.vehicle.color}</li>
                                <li>License number: ${rentalReportData.licenseNumber || 'N/A'}</li>
                            </ul>

                            <h2>Vehicle extras</h2>
                            ${this.getExtras(rentalReportData.vehicle.getExtras())}
                            
                            <h2>Client information</h2>
                            <ul>
                                <li>Age: ${rentalReportData.client.age}</li>
                            </ul>

                            <h2>Rental price</h2>
                            <ul>
                                <li>Total rental days: ${rentalReportData.rentalBill.rentalDays}</li>
                                <li>Price by day: ${rentalReportData.rentalBill.priceByDay}€</li>
                                <li>Insurance price: ${rentalReportData.rentalBill.insurancePrice}€</li>
                                <li>Total: <strong>${rentalReportData.rentalBill.total}€</strong></li>
                            </ul>
                        </body>
                    </html>`

        console.log(report)
        console.log('\n')
    }

    private getExtras(extras: Array<string>): string {
        let html = '<ul>'
        
        if (extras.length === 0) {
            html += `<li>No extras</li>`
        } else {
            for (const extra of extras) {
                html += `<li>${extra}</li>`
            }
        }

        html += '</ul>'
        return html
    }

}