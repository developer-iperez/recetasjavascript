import Client from "../../domain/Client"
import { RentalReportData } from "../../domain/interfaces/RentalReportData"
import { VehicleRentalReportRenderer } from "../../domain/interfaces/VehicleRentalReportRenderer"
import { RentalBill } from "../../domain/RentalBill"
import { Vehicle } from "../../domain/Vehicle"

export class VehicleRentalReportSimple implements VehicleRentalReportRenderer {

    public print(rentalReportData: RentalReportData) {
        let report = ''

        report += `Rental vehicle report\n`
        report += `---------------------\n`
        report += `- Vehicle information:\n`
        report += `     - Type: ${rentalReportData.vehicle.type}\n`
        report += `     - Model: ${rentalReportData.vehicle.model}\n`
        report += `     - Date: ${rentalReportData.vehicle.date}\n`
        report += `     - Color: ${rentalReportData.vehicle.color}\n`
        report += `     - License number: ${rentalReportData.licenseNumber || 'N/A'}\n`
        report += `\n`

        report += `- Vehicle extras:\n`
        if (rentalReportData.vehicle.getExtras().length === 0) {
            report += `     - No extras\n`
        } else {
            for (const extra of rentalReportData.vehicle.getExtras()) {
                report += `     - ${extra}\n`
            }
        }
        report += `\n`

        report += `- Client information:\n`
        report += `     - Age: ${rentalReportData.client.age}\n`
        report += `\n`
        
        report += `- Rental price:\n`
        report += `     - Total rental days: ${rentalReportData.rentalBill.rentalDays}\n`
        report += `     - Price by day: ${rentalReportData.rentalBill.priceByDay}€\n`
        report += `     - Insurance price: ${rentalReportData.rentalBill.insurancePrice}€\n`
        report += `     - Total: ${rentalReportData.rentalBill.total}€\n`

        console.log(report)
        console.log('\n')
    }

}