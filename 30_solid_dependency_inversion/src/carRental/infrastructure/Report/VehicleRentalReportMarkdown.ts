import { RentalReportData } from "../../domain/interfaces/RentalReportData"
import { VehicleRentalReportRenderer } from "../../domain/interfaces/VehicleRentalReportRenderer"

export class VehicleRentalReportMarkdown implements VehicleRentalReportRenderer {

    public print(rentalReportData: RentalReportData) {
        let report = ''

        report += `# Rental vehicle report\n`
        report += `## Vehicle information\n`
        report += `* Type: ${rentalReportData.vehicle.type}\n`
        report += `* Model: ${rentalReportData.vehicle.model}\n`
        report += `* Date: ${rentalReportData.vehicle.date}\n`
        report += `* Color: ${rentalReportData.vehicle.color}\n`
        report += `* License number: ${rentalReportData.licenseNumber || 'N/A'}\n`

        report += `## Vehicle extras\n`
        report += `${this.getExtras(rentalReportData.vehicle.getExtras())}\n`
                            
        report += `## Client information\n`
        report += `* Age: ${rentalReportData.client.age}\n`

        report += `## Rental price\n`
        report += `* Total rental days: ${rentalReportData.rentalBill.rentalDays}\n`
        report += `* Price by day: ${rentalReportData.rentalBill.priceByDay}€\n`
        report += `* Insurance price: ${rentalReportData.rentalBill.insurancePrice}€\n`
        report += `* Total: **${rentalReportData.rentalBill.total}€**\n`

        console.log(report)
        console.log('\n')
    }

    private getExtras(extras: Array<string>): string {
        let html = ''
        
        if (extras.length === 0) {
            html += `* No extras`
        } else {
            for (const extra of extras) {
                html += `* ${extra}`
            }
        }

        return html
    }

}