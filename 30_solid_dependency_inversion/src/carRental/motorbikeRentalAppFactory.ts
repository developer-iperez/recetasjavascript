import { VehicleRentalReportService } from "./application/Report/VehicleRentalReportService";
import { VehicleRentalService } from "./application/Shared/VehicleRentalService";
import Motorbike from "./domain/Motorbike";
import { MotorbikeRentalService } from "./domain/interfaces/MotorbikeRentalService";
import { MotorbikeReport } from "./domain/interfaces/MotorbikeReport";
import Client from "./domain/Client";
import { RentalBill } from "./domain/RentalBill";
import { RentalInformation } from "./domain/RentalInformation";
import { ReportType } from "./domain/ReportType";
import { ReportRendererFactory } from "./application/Report/ReportRendererFactory";

class MotorbikeRentalAppFacade {

    private motorbikeRentalService: MotorbikeRentalService
    private motorbikeRentalReport: MotorbikeReport

    constructor(motorbikeRentalService: MotorbikeRentalService, motorbikeRentalReport: MotorbikeReport) {
        this.motorbikeRentalService = motorbikeRentalService
        this.motorbikeRentalReport = motorbikeRentalReport
    }

    calculateBill(motorbike: Motorbike, client: Client, rentalInformation: RentalInformation): RentalBill {
        const rentalBill: RentalBill = this.motorbikeRentalService.calculateMotorbikeRental(motorbike, client, rentalInformation)
        return rentalBill
    }

    printReport(motorbike: Motorbike, client: Client, rentalBill: RentalBill) {
        this.motorbikeRentalReport.printMotorbikeReport(motorbike, client, rentalBill)
    }
}

export class MotorbikeRentalAppFactory {

    create(reportType: ReportType): MotorbikeRentalAppFacade {
        const rentalReportRenderer = ReportRendererFactory.create(reportType)
        const motorbikeRentalService = new VehicleRentalService();
        const motorbikeRentalReportService = new VehicleRentalReportService(rentalReportRenderer);
        return new MotorbikeRentalAppFacade(motorbikeRentalService, motorbikeRentalReportService);
    }
}
