import { ReportRendererFactory } from "./application/Report/ReportRendererFactory"
import { VehicleRentalReportService } from "./application/Report/VehicleRentalReportService"
import { VehicleRentalService } from "./application/Shared/VehicleRentalService"
import Bike from "./domain/Bike"
import Client from "./domain/Client"
import { BikeRentalService } from "./domain/interfaces/BikeRentalService"
import { BikeReport } from "./domain/interfaces/BikeReport"
import { RentalBill } from "./domain/RentalBill"
import { RentalInformation } from "./domain/RentalInformation"
import { ReportType } from "./domain/ReportType"

class BikeRentalAppFacade {

    private bikeRentalService: BikeRentalService
    private bikeRentalReport: BikeReport

    constructor(bikeRentalService: BikeRentalService, bikeRentalReport: BikeReport) {
        this.bikeRentalService = bikeRentalService
        this.bikeRentalReport = bikeRentalReport
    }

    calculateBill(bike: Bike, rentalInformation: RentalInformation): RentalBill {
        const rentalBill: RentalBill = this.bikeRentalService.calculateBikeRental(bike, rentalInformation)
        return rentalBill
    }

    printReport(bike: Bike, client: Client, rentalBill: RentalBill) {
        this.bikeRentalReport.printBikeReport(bike, client, rentalBill)
    }
}

export class BikeRentalAppFactory {
    create(reportType: ReportType): BikeRentalAppFacade {
        const rentalReportRenderer = ReportRendererFactory.create(reportType)
        const bikeRentalService = new VehicleRentalService()
        const bikeRentalReportService = new VehicleRentalReportService(rentalReportRenderer)
        return new BikeRentalAppFacade(bikeRentalService, bikeRentalReportService)
    }
}
