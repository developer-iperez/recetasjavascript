function arraysBasics() {
    let collections: string[] = ['Led Zeppelin', 'Deep Purple', 'AC/DC'];
    console.log(`Music collection (${collections.length}) -> ${collections}`);
}

function tuple() {
    let userInformation: [number, string, boolean] = [1, 'user1', true];
    console.log(`User (tuple) -> id=${userInformation[0]}, name=${userInformation[1]}, enabled=${userInformation[2]}`);
}

function tupleArray() {
    let users: [number, string, boolean][] = [[1, 'user1', true], [2, 'user2', false]];
    users.forEach((user) => {
        console.log(`User (tuple) -> id=${user[0]}, name=${user[1]}, enabled=${user[2]}`);
    });
}

function enumBasics() {
    enum UserType { Administrator, Mantainer, Reader };
    let userInformation: [number, string, boolean, UserType] = [1, 'user1', true, UserType.Administrator];
    
    console.log(`User (enum) -> 
        id=${userInformation[0]}, 
        name=${userInformation[1]}, 
        enabled=${userInformation[2]},
        type=${userInformation[3]}`);
}

function enumString() {
    enum UserType { Administrator = 'Administrator', Maintainer = 'Maintainer', Reader = 'Reader' };
    let userInformation: [number, string, boolean, UserType] = [1, 'user1', true, UserType.Administrator];
    
    console.log(`User (enum) -> 
        id=${userInformation[0]}, 
        name=${userInformation[1]}, 
        enabled=${userInformation[2]},
        type=${userInformation[3]}`);
}

function alias() {
    type UserAlias = [number, string, boolean];
    let userInformation: UserAlias = [1, 'user1', true];

    console.log(`User (alias) -> 
        id=${userInformation[0]}, 
        name=${userInformation[1]}, 
        enabled=${userInformation[2]}`);
}

export function arrayExamples() {
    console.log('\nTypeScript Array Examples\n-------------------------');

    console.log('\nArray basics:');
    arraysBasics();

    console.log('\nTuple:');
    tuple();

    console.log('\nTuple array:');
    tupleArray();

    console.log('\nEnum:');
    enumBasics();
    enumString();

    console.log('\nAlias:');
    alias();
}

