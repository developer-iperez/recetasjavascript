function inferredTypesExample() {
    console.log('\nTipos inferidos...');
    let variableWithoutType;
    console.log(typeof variableWithoutType);    // Salida -> undefined
    variableWithoutType = 'string';
    console.log(typeof variableWithoutType);    // Salida -> string
    variableWithoutType = 123;
    console.log(typeof variableWithoutType);    // Salida -> number
    variableWithoutType = true;
    console.log(typeof variableWithoutType);    // Salida -> boolean

    let value1 = 10;
    let value2 = '10';
    console.log(`Resultado: ${value1 + value2}`);                   // Salida -> 1010
    console.log(`Resultado: ${value1 + parseInt(value2)}`);         // Salida -> 20
}

function basicTypesExample() {
    console.log('\nTipos explícitos...');
    let variablewithType: string;
    // COMPILATION ERROR -> console.log(typeof variablewithType);
    variablewithType = 'Cadena de texto';
    console.log(typeof variablewithType);
    // COMPILATION ERROR -> variablewithType = 123;
    // COMPILATION ERROR -> variablewithType = true;

    let variablewithTypeNumber: number;
    // COMPILATION ERROR -> console.log(typeof variablewithTypeNumber);
    variablewithTypeNumber = 123;
    console.log(typeof variablewithTypeNumber);
    // COMPILATION ERROR -> variablewithTypeNumber = 'Cadena de texto';
    // COMPILATION ERROR -> variablewithTypeNumber = true;

    let variablewithTypeBoolean: boolean;
    // COMPILATION ERROR -> console.log(typeof variablewithTypeBoolean);
    variablewithTypeBoolean = true;
    console.log(typeof variablewithTypeBoolean);
    // COMPILATION ERROR -> variablewithTypeBoolean = 'Cadena de texto';
    // COMPILATION ERROR -> variablewithTypeBoolean = 123;
}

function anyTypeExample() {
    console.log('\nEjemplos con tipo "any"...');
    let myType: any;
    myType = 'Value like string';
    console.log(typeof myType);     // Salida -> string
    myType = 3.1415;
    console.log(typeof myType);     // Salida -> number
    myType = true;
    console.log(typeof myType);     // Salida -> boolean
}

function unionExample() {
    console.log('\nEjemplo con tipo "union"...');

    let now: Date | string;
    
    now = new Date();
    let nowAsDate = now as Date;
    console.log(`Variable como clase Date: ${nowAsDate}`);
    // COMPILATION ERROR -> now = true;

    now = now.toLocaleString();
    let nowAsString = now as string;
    console.log(`Variable como cadena: ${nowAsString}`);
    // COMPILATION ERROR -> now = 1234;
}

export function typesExamples() {
    console.log('\nTypeScript Basic Types Examples\n-------------------------------');

    inferredTypesExample();
    basicTypesExample();
    anyTypeExample();
    unionExample();
};
