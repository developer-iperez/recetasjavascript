function exampleWithOptionalParameter(parameter?: any) {
    console.log(`Optional parameter: ${parameter || '...parameter not defined'}`);
}

function exampleWithParameterWithDefaultValue(parameter: string = '...the default value!') {
    console.log(`Parameter with default value: ${parameter}`);
}

function exampleWithRestParameter(...restParameter: any[]) {
    if (restParameter.length === 0) {
        console.log('No \'Rest\' parameter...');
    }
    else {
        console.log(`I\'m sure you pass me a total number of ${restParameter.length} parameters`);
    }
}

function exampleWithoutReturnValue(): void {
    console.log('... nothing!');
}

export function functionExamples() {
    console.log('\nTypeScript Function Examples\n----------------------------');
    
    console.log('\nOptional parameters:');
    exampleWithOptionalParameter();
    exampleWithOptionalParameter('Hello, I\'m a parameter');

    console.log('\nParameters with default values:');
    exampleWithParameterWithDefaultValue();
    exampleWithParameterWithDefaultValue('Hello, I\'m a parameter');

    console.log('\nRest parameter:');
    exampleWithRestParameter();
    exampleWithRestParameter('John');
    exampleWithRestParameter('John', 'Peterson');

    console.log('\nFunction without return value:');
    console.log(exampleWithoutReturnValue());
}
