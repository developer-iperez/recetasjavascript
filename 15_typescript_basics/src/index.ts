import { helloWorld } from './hello-world';
import { typesExamples } from './types';
import { functionExamples } from './functions';
import { arrayExamples } from './arrays';

helloWorld('Typescript');
typesExamples();
functionExamples();
arrayExamples();
