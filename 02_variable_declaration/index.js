var fruit1 = 'apple';

if (fruit1 === 'apple') {
  var fruit1 = 'banana';
  console.log('fruit: ' + fruit1);    // banana
}

console.log('fruit: ' + fruit1);      // banana

////////////////////////////////////////////////

let fruit2 = 'apple';

if (fruit2 === 'apple') {
  let fruit2 = 'banana';
  console.log('fruit2: ' + fruit2);      // "banana"
}

console.log('fruit2: ' + fruit2);        // "apple"

////////////////////////////////////////////////

const fruit3 = 'apple';

try {
    fruit3 = 'banana';                  // Una excepción o error es lanzado en este punto...
} catch(exeption) {
    console.log('Error: It\'s not possible to reassign the value of a constant');
}

console.log('fruit3: ' + fruit3);       // "apple"
