import { EUR, CurrencyLibrary } from './library.js'

const currenciesUtilityClass = new CurrencyLibrary()
console.log(`Currency library information:\n${currenciesUtilityClass}\n`)

let info = currenciesUtilityClass.getCurrency(EUR)
console.log(`Currency information for Symbol EUR => ${info}`)

try {
    info = currenciesUtilityClass.getCurrency('EUR')
    console.log(`Currency information for string 'EUR' => ${info}`)
}
catch (error) {
    console.log(error.message)
}

console.log('\nAll the available library currencies:')
for(const currency of currenciesUtilityClass) {
    console.log(`\t${currency}`)
}
