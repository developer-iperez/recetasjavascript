const metadataInformationUpdateDate = Symbol('informationDate')
const metadataVersion = Symbol('version')
const libraryCreationDate = '01/01/2021'
const libraryVersion = '1.0'

const ID = Symbol('id')
const EUR = Symbol()
const USD = Symbol()
const GBP = Symbol()
const JPY = Symbol()
const BTC = Symbol()
const LTC = Symbol()
const ETH = Symbol()
const ADA = Symbol()

const data = [
    {  
        id: EUR,
        code: 'eur',
        symbol: '€',
        name: 'Euro'
    },
    {   
        id: USD,
        code: 'usd',
        symbol: '$',
        name: 'Dollar'
    },
    {   
        id: GBP,
        code: 'gbp',
        symbol: '£',
        name: 'Pound Sterling'
    },
    {   
        id: JPY,
        code: 'jpy',
        symbol: '¥',
        name: 'Japanese Yen'
    },
    {  
        id: BTC,
        code: 'btc',
        symbol: '₿',
        name: 'Bitcoin'
    },
    {   
        id: LTC,
        code: 'ltc',
        symbol: 'Ł',
        name: 'Litecoin'
    },
    {   
        id: ETH,
        code: 'eth',
        symbol: 'ETH',
        name: 'Ethereum'
    },
    {   
        id: ADA,
        code: 'ada',
        symbol: '₳',
        name: 'Cardano'
    }
]

function symbolIterator(currencies) {
    let index = 0

    return {
        next() {
            return {
                value: currencies[index],
                done: index++ >= currencies.length,
            }
        }
    }
}

class Currency {
    constructor(data) {
        this[ID] = data?.id
        this.code = data?.code
        this.symbol = data?.symbol
        this.name = data?.name
    }

    toString() {
        return `Currency ${this.code}: ${this.symbol} (${this.name})`
    }
}

class CurrencyLibrary {
    constructor() {
        this[metadataInformationUpdateDate] = libraryCreationDate
        this[metadataVersion] = libraryVersion
        this[Symbol.iterator] = () => symbolIterator(this.data)

        this.data = data.map(x => {
            return new Currency(x)
        })
    }

    currencies() {
        return this.data.map(x => x.code)
    }

    getCurrency(id) {
        if (!!id === false)
            throw new Error('Invalid arguments')

        const currencyDescription =  this.data.find(x => x[ID] === id)
        if (currencyDescription === undefined)
            throw new Error(`Not found a valid object for currency ${id}`)
        
        return currencyDescription
    }

    toString() {
        return  `\tAvailable currencies: ${this.currencies()}\n` +
                `\t[Metadata] Library date: ${this[metadataInformationUpdateDate]}\n` + 
                `\t[Metadata] Version: ${this[metadataVersion]}`
    }
}

export {
    EUR,
    USD,
    GBP,
    JPY,
    BTC,
    LTC,
    ETH,
    ADA,
    CurrencyLibrary
}