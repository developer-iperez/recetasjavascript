let user = {
    name: 'Gaal',
    surname: 'Dornick',
    age: 51,
    male: true,
    occupation: ['Mathematics', 'Psychohistory'],
    bio: () => {
        return `Gaal Dornick was a psychohistorian who came to Trantor in 19482 to become a member of Hari Seldon's mysterious Seldon Project.`;
    },
    'favourite-novel': {
        title: 'Foundation',
        author: 'Isaac Asimov'
    }
}

// Output: "Name: Gaal"
console.log(`Name: ${user.name}`);

// Output: "Surname: Dornick"
const surnameKey = 'surname';
console.log(`Surname: ${user[surnameKey]}`);

// Output: "Age: 51"
const { age } = user;
console.log(`Age: ${age}`);

// Output: "GenreIsMale: true"
const { male: genreIsMale } = user;
console.log(`GenreIsMale: ${genreIsMale}`);

// Output: "Jobs: Mathematics,Psychohistory"
const occupationKey = 'occupation';
const { [occupationKey]: jobs } = user;
console.log(`Jobs: ${jobs}`);

// Output: "Favourite novel:"
//            "Author: Isaac Asimov"
//            "Title : Foundation"
console.log('Favourite novel:');
console.log('  Author: ' + user['favourite-novel'].author);
console.log('  Title : ' + user['favourite-novel']['title']);
