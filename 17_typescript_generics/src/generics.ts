import Film from './classes/Film';
import FilmDatabase from './classes/FilmDatabase';
import Song from './classes/Song';
import SongDatabase from './classes/SongDatabase';
import GenericDatabase from './classes/GenericDatabase';
import OnlineVendor from './classes/OnlineVendor';
import GenericSalesDatabase from './classes/GenericSalesDatabase';

function getSongList(): Array<Song> {
    return [
        new Song(0, 'Like a Rolling Stone', 'Bob Dylan'),
        new Song(1, '(I Can\'t Get No) Satisfaction', 'The Rolling Stones'),
        new Song(2, 'Imagine', 'John Lennon'),
        new Song(3, 'What\'s Going On', 'Marvin Gaye')
    ];
}

function getFilmList(): Array<Film> {
    return [
        new Film(0, 'The Godfather', 'Francis Ford Coppola'),
        new Film(1, 'The Wizard of Oz', 'Victor Fleming'),
        new Film(2, 'Citizen Kane', 'Orson Welles'),
        new Film(3, 'The Shawshank Redemption', 'Frank Darabont')
    ];
}

function exampleWithoutGenerics() {
    let songDatabase: SongDatabase;
    let filmDatabase: FilmDatabase;

    songDatabase = new SongDatabase();
    getSongList().forEach((item) => {
        songDatabase.add(item);
    });

    filmDatabase = new FilmDatabase();
    getFilmList().forEach((item) => {
        filmDatabase.add(item);
    });

    console.log('Databases without generics:');
    console.log('---------------------------');
    console.log(songDatabase.toString());
    console.log(`Getting song 1: ${songDatabase.get(1)}`);
    console.log(filmDatabase.toString());
    console.log(`Getting film 2: ${filmDatabase.get(2)}`);
}

function exampleWithGenerics() {
    let songDatabase: GenericDatabase<Song>;
    let filmDatabase: GenericDatabase<Film>;

    songDatabase = new GenericDatabase<Song>();
    getSongList().forEach((item) => {
        songDatabase.add(item);
    });

    filmDatabase = new GenericDatabase<Film>();
    getFilmList().forEach((item) => {
        filmDatabase.add(item);
    });

    console.log('\nDatabases with generics:');
    console.log('---------------------------');
    console.log(songDatabase.toString());
    console.log(`Getting song 1: ${songDatabase.get(1)}`);
    console.log(filmDatabase.toString());
    console.log(`Getting film 2: ${filmDatabase.get(2)}`);
}

function exampleWithMultipleGenerics() {
    let vendor = new OnlineVendor('https://www.amazon.com/');
    let film = new Film(1, '2001: A Space Odyssey', 'Stanley Kubrick');
    let sales = new GenericSalesDatabase<OnlineVendor, Film>();

    sales.addSale(vendor, film);

    console.log('\nDatabases with multiple generics:');
    console.log('-----------------------------------');
    console.log(`Vendor sales (${vendor.site}):`);
    console.log(sales.getSales(vendor)?.toString());
}

export function genericsExamples() {
    console.log('\nTypeScript Generics (2) Examples\n-------------------');

    exampleWithoutGenerics();
    exampleWithGenerics();
    exampleWithMultipleGenerics();
}
