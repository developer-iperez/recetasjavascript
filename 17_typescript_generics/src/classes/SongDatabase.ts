import Song from './Song';

export default class FilmDatabase {
    private list: Array<Song>;
    
    constructor() {
        this.list = new Array<Song>();
    }

    public add(item: Song): void {
        this.list.push(item);
    }

    public remove(id: number): void {
        let index: number  = this.list.findIndex((o) => { return o.getId() === id });
        if (index > -1) {
            this.list.splice(index, 1);
        }
    }

    public get(id: number): Song | undefined {
        let item: Song | undefined = this.list.find((o) => { return o.getId() === id });
        return item;
    }

    public toString(): string {
        return this.list.toString();
    }
}