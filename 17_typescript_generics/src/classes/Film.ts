import IIdentity from '../interfaces/IIdentity'; 

export default class Film implements IIdentity {
    private id: number;
    private title: string;
    private director: string;

    constructor(id: number, title: string, director: string) {
        this.id = id;
        this.title = title;
        this.director = director;
    }

    public getId(): number {
        return this.id;
    }

    public getTitle(): string {
        return this.title;
    }

    public setTitle(title: string): void {
        this.title = title;
    }

    public getDirector(): string {
        return this.director;
    }

    public setDirector(director: string): void {
        this.director = director;
    }

    public toString(): string {
        return `[${this.id}, title=${this.title}, director=${this.director}]`;
    }
}