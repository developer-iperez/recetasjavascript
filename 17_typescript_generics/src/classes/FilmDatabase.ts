import Film from './Film';

export default class FilmDatabase {
    private list: Array<Film>;
    
    constructor() {
        this.list = new Array<Film>();
    }

    public add(item: Film): void {
        this.list.push(item);
    }

    public remove(id: number): void {
        let index: number  = this.list.findIndex((o) => { return o.getId() === id });
        if (index > -1) {
            this.list.splice(index, 1);
        }
    }

    public get(id: number): Film | undefined {
        let item: Film | undefined = this.list.find((o) => { return o.getId() === id });
        return item;
    }

    public toString(): string {
        return this.list.toString();
    }
}