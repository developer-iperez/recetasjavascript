import IIdentity from '../interfaces/IIdentity';

export default class GenericDatabase<T extends IIdentity> {
    private list: Array<T>;
    
    constructor() {
        this.list = new Array<T>();
    }

    public add(item: T): void {
        this.list.push(item);
    }

    public remove(id: number): void {
        let index: number  = this.list.findIndex((o) => { return o.getId() === id });
        if (index > -1) {
            this.list.splice(index, 1);
        }
    }

    public get(id: number): T | undefined {
        let item: T | undefined = this.list.find((o) => { return o.getId() === id });
        return item;
    }

    public toString(): string {
        return this.list.toString();
    }
}