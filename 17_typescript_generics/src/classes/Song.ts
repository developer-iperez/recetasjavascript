import IIdentity from '../interfaces/IIdentity'; 

export default class Song implements IIdentity {
    private id: number;
    private title: string;
    private artist: string;

    constructor(id: number, title: string, artist: string) {
        this.id = id;
        this.title = title;
        this.artist = artist;
    }

    public getId(): number {
        return this.id;
    }

    public getTitle(): string {
        return this.title;
    }

    public setTitle(title: string): void {
        this.title = title;
    }

    public getArtist(): string {
        return this.artist;
    }

    public setArtist(artist: string): void {
        this.artist = artist;
    }

    public toString(): string {
        return `[${this.id}, title=${this.title}, artist=${this.artist}]`;
    }
}