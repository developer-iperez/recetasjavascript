export default class OnlineVendor {
    private _site: string;

    constructor(site: string) {
        this._site = site;
    }

    get site(): string {
        return this._site;
    }

    set site(value: string) {
        this._site = value;
    }
}