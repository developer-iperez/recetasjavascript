import OnlineVendor from './OnlineVendor';
import Film from './Film';
import Song from './Song'; 

export default class GenericSalesDatabase<K extends OnlineVendor, V extends Film | Song> {

    private keys: Array<K>;
    private values: Array<V>;

    constructor() {
        this.keys = new Array<K>();
        this.values = new Array<V>();
    }

    addSale(key: K, value: V): void {
        this.keys.push(key);
        this.values.push(value);
    }

    getSales(key: K): V | null {
        const index: number = this.keys.indexOf(key);
        return (index >= 0) ? this.values[index] : null;
    } 
}
