export default interface IIdentity {
    getId(): number;
}