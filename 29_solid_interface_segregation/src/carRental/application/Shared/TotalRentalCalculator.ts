import { RentalBill } from "../../domain/RentalBill";

export class TotalRentalCalculator {

    calculate(rentalBill: RentalBill): number {
        return (rentalBill.rentalDays * rentalBill.priceByDay) + rentalBill.insurancePrice;
    }

}
