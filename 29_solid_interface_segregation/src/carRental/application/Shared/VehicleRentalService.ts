import Bike from "../../domain/Bike"
import Car from "../../domain/Car"
import Client from "../../domain/Client"
import Motorbike from "../../domain/Motorbike"
import { RentalBill } from "../../domain/RentalBill"
import { RentalInformation } from "../../domain/RentalInformation"
import { BikeRentalService } from "../../domain/interfaces/BikeRentalService"
import { CarRentalService } from "../../domain/interfaces/CarRentalService"
import { MotorbikeRentalService } from "../../domain/interfaces/MotorbikeRentalService"
import { CarInsurancePrice } from "../Car/CarInsurancePrice"
import { MotorbikeInsurancePrice } from "../Motorbike/MotorbikeInsurancePrice"
import { CarRentalPrice } from "../Car/CarRentalPrice"
import { BikeRentalPrice } from "../Bike/BikeRentalPrice"
import { MotorbikeRentalPrice } from "../Motorbike/MotorbikeRentalPrice"
import { RentalBillService } from "./RentalBillService"

export class VehicleRentalService implements CarRentalService, MotorbikeRentalService, BikeRentalService {

    private rentalBillService: RentalBillService 

    constructor() {
        this.rentalBillService = new RentalBillService()
    }

    calculateCarRental(car: Car, client: Client, rentalInformation: RentalInformation): RentalBill {
        const carRentalPrice = new CarRentalPrice(car)
        const carInsurancePrice = new CarInsurancePrice(car, client)

        return this.rentalBillService.getRentalBill(carRentalPrice.calculate(), carInsurancePrice.calculate(), rentalInformation)
    }

    calculateMotorbikeRental(motorbike: Motorbike, client: Client, rentalInformation: RentalInformation): RentalBill {
        const motorbikeRentalPrice = new MotorbikeRentalPrice(motorbike)
        const motorbikeInsurancePrice = new MotorbikeInsurancePrice(motorbike, client)

        return this.rentalBillService.getRentalBill(motorbikeRentalPrice.calculate(), motorbikeInsurancePrice.calculate(), rentalInformation)
    }

    calculateBikeRental(bike: Bike, rentalInformation: RentalInformation): RentalBill {
        const motorbikeRentalPrice = new BikeRentalPrice(bike)

        return this.rentalBillService.getRentalBill(motorbikeRentalPrice.calculate(), 0, rentalInformation)
    }
}
