import Bike from "../../domain/Bike";
import Car from "../../domain/Car";
import Client from "../../domain/Client";
import Motorbike from "../../domain/Motorbike";
import { RentalBill } from "../../domain/RentalBill";
import { Vehicle } from "../../domain/Vehicle";
import { BikeReport } from "../../domain/interfaces/BikeReport";
import { CarReport } from "../../domain/interfaces/CarReport";
import { MotorbikeReport } from "../../domain/interfaces/MotorbikeReport";

export class VehicleRentalReport implements CarReport, MotorbikeReport, BikeReport {

    public printCarReport(car: Car, client: Client, rentalBill: RentalBill): void {
        let report = ''
        report += this.printMainPart(car, car.licenseNumber)
        report += this.printExtras(car)
        report += this.printClientPart(client)
        report += this.printRentalResumePart(rentalBill, true)
        console.log(report)
    }

    public printMotorbikeReport(motorbike: Motorbike, client: Client, rentalBill: RentalBill): void {
        let report = ''
        report += this.printMainPart(motorbike, motorbike.licenseNumber)
        report += this.printExtras(motorbike)
        report += this.printClientPart(client)
        report += this.printRentalResumePart(rentalBill, true)
        console.log(report)
    }

    public printBikeReport(bike: Bike, rentalBill: RentalBill): void {
        let report = ''
        report += this.printMainPart(bike, '')
        report += this.printExtras(bike)
        report += this.printRentalResumePart(rentalBill, false)
        console.log(report)
    }

    private printMainPart(vehicle: Vehicle, licenseNumber: string) {
        let reportPart = ''
        reportPart += `Rental vehicle report\n`
        reportPart += `-----------------\n`
        reportPart += `- Vehicle information:\n`
        reportPart += `     - Type: ${vehicle.type}\n`
        reportPart += `     - Model: ${vehicle.model}\n`
        reportPart += `     - Date: ${vehicle.date}\n`
        reportPart += `     - Color: ${vehicle.color}\n`
        reportPart += `     - License number: ${licenseNumber || 'N/A'}\n`
        reportPart += `\n`
        return reportPart
    }

    private printExtras(vehicle: Vehicle) {
        let reportPart = ''
        reportPart += `- Vehicle extras:\n`
        if (vehicle.getExtras().length === 0) {
            reportPart += `     - No extras\n`
        } else {
            for (const extra of vehicle.getExtras()) {
                reportPart += `     - ${extra}\n`
            }
        }
        reportPart += `\n`
        return reportPart
    }

    private printClientPart(client: Client) {
        let reportPart = ''
        reportPart += `- Client information:\n`
        reportPart += `     - Age: ${client.age}\n`
        reportPart += `\n`
        return reportPart
    }

    private printRentalResumePart(rentalBill: RentalBill, hasInsurancePrice: boolean) {
        let reportPart = ''
        reportPart += `- Rental price:\n`
        reportPart += `     - Total rental days: ${rentalBill.rentalDays}\n`
        reportPart += `     - Price by day: ${rentalBill.priceByDay}€\n`
        if (hasInsurancePrice)
            reportPart += `     - Insurance price: ${rentalBill.insurancePrice}€\n`
        reportPart += `     - Total: ${rentalBill.total}€\n`
        return reportPart
    }
}
