import Client from "../Client";
import Motorbike from "../Motorbike";
import { RentalBill } from "../RentalBill";
import { RentalInformation } from "../RentalInformation";

export interface MotorbikeRentalService {
    calculateMotorbikeRental(motorbike: Motorbike, client: Client, rentalInformation: RentalInformation): RentalBill;
}
