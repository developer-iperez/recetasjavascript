import Bike from "../Bike";
import { RentalBill } from "../RentalBill";
import { RentalInformation } from "../RentalInformation";

export interface BikeRentalService {
    calculateBikeRental(bike: Bike, rentalInformation: RentalInformation): RentalBill;
}
