export interface VehicleRentalPrice {
    calculate(): number;
}
