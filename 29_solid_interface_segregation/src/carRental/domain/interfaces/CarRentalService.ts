import Car from "../Car";
import Client from "../Client";
import { RentalBill } from "../RentalBill";
import { RentalInformation } from "../RentalInformation";

export interface CarRentalService {
    calculateCarRental(car: Car, client: Client, rentalInformation: RentalInformation): RentalBill;
}
