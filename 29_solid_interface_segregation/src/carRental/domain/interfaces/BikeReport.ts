import Bike from "../Bike";
import { RentalBill } from "../RentalBill";

export interface BikeReport {
    printBikeReport(bike: Bike, rentalBill: RentalBill): void;
}
