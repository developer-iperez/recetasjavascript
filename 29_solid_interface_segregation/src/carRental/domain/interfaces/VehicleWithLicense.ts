export interface VehicleWithLicense {
    get licenseNumber();
    set licenseNumber(licenseNumber: string);
}
