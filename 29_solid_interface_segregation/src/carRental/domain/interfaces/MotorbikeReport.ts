import Client from "../Client";
import Motorbike from "../Motorbike";
import { RentalBill } from "../RentalBill";

export interface MotorbikeReport {
    printMotorbikeReport(motorbike: Motorbike, client: Client, rentalBill: RentalBill): void;
}
