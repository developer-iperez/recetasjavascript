import Car from "../Car";
import Client from "../Client";
import { RentalBill } from "../RentalBill";

export interface CarReport {
    printCarReport(car: Car, client: Client, rentalBill: RentalBill): void;
}
