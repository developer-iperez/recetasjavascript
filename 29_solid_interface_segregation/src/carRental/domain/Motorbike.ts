import { Vehicle } from "./Vehicle"
import { VehicleWithLicense } from "./interfaces/VehicleWithLicense"

export default class Motorbike extends Vehicle implements VehicleWithLicense {

    private _includesHelmet: boolean
    private _licenseNumber: string

    constructor() {
        super('Motorbike')
        this._licenseNumber = ''
        this._includesHelmet = false
    }

    get licenseNumber() {
        return this._licenseNumber;
    }

    set licenseNumber(licenseNumber: string) {
        this._licenseNumber = licenseNumber;
    }

    public get includeHelmet() {
        return this._includesHelmet
    }

    public set includeHelmet(includesHelmet: boolean) {
        this._includesHelmet = includesHelmet
    }

    getExtras(): Array<string> {
        let extras = []

        if (this.includeHelmet)
            extras.push('Helmet')

        return extras
    }
}