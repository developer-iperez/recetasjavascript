import { Vehicle } from "./Vehicle"
import { VehicleWithLicense } from "./interfaces/VehicleWithLicense"

export default class Car extends Vehicle implements VehicleWithLicense {

    private _includesChildSeats: boolean
    private _licenseNumber: string

    constructor() {
        super('Car')
        this._licenseNumber = ''
        this._includesChildSeats = false
    }

    get licenseNumber() {
        return this._licenseNumber;
    }

    set licenseNumber(licenseNumber: string) {
        this._licenseNumber = licenseNumber;
    }

    public get includesChildSeats(): boolean {
        return this._includesChildSeats
    }

    public set includesChildSeats(includesChildSeats: boolean) {
        this._includesChildSeats = includesChildSeats
    }

    getExtras(): Array<string> {
        let extras = []

        if (this.includesChildSeats)
            extras.push('Child seats')

        return extras
    }

}