import { VehicleRentalReport } from "./application/Report/VehicleRentalReport"
import { VehicleRentalService } from "./application/Shared/VehicleRentalService"
import Bike from "./domain/Bike"
import Car from "./domain/Car"
import Client from "./domain/Client"
import { BikeRentalService } from "./domain/interfaces/BikeRentalService"
import { BikeReport } from "./domain/interfaces/BikeReport"
import { CarRentalService } from "./domain/interfaces/CarRentalService"
import { CarReport } from "./domain/interfaces/CarReport"
import { MotorbikeRentalService } from "./domain/interfaces/MotorbikeRentalService"
import { MotorbikeReport } from "./domain/interfaces/MotorbikeReport"
import Motorbike from "./domain/Motorbike"
import { RentalBill } from "./domain/RentalBill"
import { RentalInformation } from "./domain/RentalInformation"

export class CarRentalApp {

    private carRentalService: CarRentalService
    private carRentalReport: CarReport

    constructor() {
        this.carRentalService = new VehicleRentalService()   
        this.carRentalReport = new VehicleRentalReport()
    }

    calculateBill(car: Car, client: Client, rentalInformation: RentalInformation): RentalBill {
        const rentalBill: RentalBill = this.carRentalService.calculateCarRental(car, client, rentalInformation)
        return rentalBill
    }

    printReport(car: Car, client: Client, rentalBill: RentalBill) {
        this.carRentalReport.printCarReport(car, client, rentalBill)
    }
}

export class MotorbikeRentalApp {

    private motorbikeRentalService: MotorbikeRentalService
    private motorbikeRentalReport: MotorbikeReport

    constructor() {
        this.motorbikeRentalService = new VehicleRentalService()
        this.motorbikeRentalReport = new VehicleRentalReport()
    }

    calculateBill(motorbike: Motorbike, client: Client, rentalInformation: RentalInformation): RentalBill {
        const rentalBill: RentalBill = this.motorbikeRentalService.calculateMotorbikeRental(motorbike, client, rentalInformation)
        return rentalBill
    }

    printReport(motorbike: Motorbike, client: Client, rentalBill: RentalBill) {
        this.motorbikeRentalReport.printMotorbikeReport(motorbike, client, rentalBill)
    }
}

export class BikeRentalApp {

    private bikeRentalService: BikeRentalService
    private bikeRentalReport: BikeReport

    constructor() {
        this.bikeRentalService = new VehicleRentalService()
        this.bikeRentalReport = new VehicleRentalReport()
    }

    calculateBill(bike: Bike, rentalInformation: RentalInformation): RentalBill {
        const rentalBill: RentalBill = this.bikeRentalService.calculateBikeRental(bike, rentalInformation)
        return rentalBill
    }

    printReport(bike: Bike, rentalBill: RentalBill) {
        this.bikeRentalReport.printBikeReport(bike, rentalBill)
    }
}
