import { BikeRentalApp, CarRentalApp, MotorbikeRentalApp } from './carRental/app'
import Bike from './carRental/domain/Bike'
import Car from './carRental/domain/Car'
import Client from './carRental/domain/Client'
import Motorbike from './carRental/domain/Motorbike'
import { RentalInformation } from './carRental/domain/RentalInformation'

function calculateCarRentalDemo() {
    const car = new Car()
    car.model = 'SuperCar'
    car.date = new Date(2015, 4, 1)
    car.color = 'black'
    car.licenseNumber = 'ABC1234'
    car.includesChildSeats = true

    const client = new Client()
    client.age = 20 

    const rentalInformation = new RentalInformation()
    rentalInformation.rentalDays = 3

    const rentalApp = new CarRentalApp()
    const rentalBill = rentalApp.calculateBill(car, client, rentalInformation)
    rentalApp.printReport(car, client, rentalBill)
}

function calculateMotorbikeRentalDemo() {
    const motorbike = new Motorbike()
    motorbike.model = 'SuperMotorBike'
    motorbike.date = new Date(2015, 4, 1)
    motorbike.color = 'black'
    motorbike.licenseNumber = 'ABC56789'

    const client = new Client()
    client.age = 20 

    const rentalInformation = new RentalInformation()
    rentalInformation.rentalDays = 3

    const rentalApp = new MotorbikeRentalApp()
    const rentalBill = rentalApp.calculateBill(motorbike, client, rentalInformation)
    rentalApp.printReport(motorbike, client, rentalBill)
}

function calculateBikeRentalDemo() {
    const bike = new Bike()
    bike.model = 'SuperBike'
    bike.date = new Date(2015, 4, 1)
    bike.color = 'black'

    const rentalInformation = new RentalInformation()
    rentalInformation.rentalDays = 3

    const rentalApp = new BikeRentalApp()
    const rentalBill = rentalApp.calculateBill(bike, rentalInformation)
    rentalApp.printReport(bike, rentalBill)
}

calculateCarRentalDemo()
calculateMotorbikeRentalDemo()
calculateBikeRentalDemo()
