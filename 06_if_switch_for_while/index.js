function isValidDate(item) {
    let isValidDate = true;

    if (item.length === 10) {
        for (let i = 0; i < item.length; i++) {       
            switch(item[i]) {
                case '-':
                    if (i !== 4 && i !== 7) {
                        isValidDate = false;
                    }
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    // Valid value
                    break;
                default:
                    isValidDate = false;
                    break;
            }
    
            if (isValidDate === false) {
                break;
            }
        }
    }
    else {
        isValidDate = false;
    }

    return isValidDate;
}

console.log('Expected: false, Result: ' + isValidDate('2030'));
console.log('Expected: false, Result: ' + isValidDate('2030-12'));
console.log('Expected: false, Result: ' + isValidDate('2030-12-0'));
console.log('Expected: false, Result: ' + isValidDate('2-12-06'));
console.log('Expected: false, Result: ' + isValidDate('203O-12-06'));
console.log('Expected: true, Result: ' + isValidDate('2030-12-06'));
