const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

const basePath = 'dist/library'
const isProduction = process.env.NODE_ENV == 'production';

const libraryConfig = {
    // Name of the configuration, shown in output
    name: 'numberSequencelibrary',
    // Here the application starts executing and webpack starts bundling (default ./src)
    entry: {
        numberSequenceLib: './src/numberSequenceLib/index.ts'
    },
    // Options related to how webpack emits results
    output: {
        // The target directory for all output files must be an absolute path
        path: path.resolve(__dirname, basePath),
        // The filename template for entry chunks
        filename: '[name].js',
        // Output a library exposing the exports of your entry point.
        library: 'NumberSequenceLib'
    },
    // List of plugins
    plugins: [
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: [path.resolve(process.cwd(), `${basePath}/*`)]
        })
    ],
    // Configuration regarding modules
    module: {
        // Rules for modules (configure loaders, parser options, etc.)
        rules: [
            {
                // Condition
                test: /\.(ts|tsx)$/i,
                exclude: ['/node_modules/'],
                // The loader which should be applied, it'll be resolved relative to the context
                loader: 'ts-loader'
            },

            // Add your rules for custom modules here
            // Learn more about loaders from https://webpack.js.org/loaders/
        ],
    },
    // Options for resolving module requests
    resolve: {
        // Extensions that are used
        extensions: ['.tsx', '.ts'],
    },
};

// Chosen mode tells webpack to use its built-in optimizations accordingly. ("production" | "development" | "none")
if (isProduction) {
    libraryConfig.mode = 'production';
} else {
    libraryConfig.mode = 'development';
}

module.exports = libraryConfig
