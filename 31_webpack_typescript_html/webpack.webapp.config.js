const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const isProduction = process.env.NODE_ENV == 'production'
const basePath = 'dist/webapp'
const stylesHandler = MiniCssExtractPlugin.loader

const webappConfig = {
    // Name of the configuration, shown in output
    name: 'webapp',
    // Here the application starts executing and webpack starts bundling (default ./src)
    entry: {
        webapp: './src/webapp/index.ts'
    },
    // Options related to how webpack emits results
    output: {
        // The target directory for all output files must be an absolute path
        path: path.resolve(__dirname, basePath),
        // The filename template for entry chunks
        filename: '[name].js',
        // Output a library exposing the exports of your entry point.
        library: '[name]',
        // It allows you to specify the base path for all the assets within your application
        publicPath: './',
        // Custom output filename for assets/resources
        assetModuleFilename: 'assets/images/[name][ext][query]'
    },
    // List of plugins
    plugins: [
        // Simplifies creation of HTML files to serve your webpack bundles.
        new HtmlWebpackPlugin({
            title: 'webapp',
            template: './src/webapp/index.html',
            filename: './index.html',
            inject: false
        }),

        // This plugin extracts CSS into separate files. It creates a CSS file per JS file which contains CSS. It supports On-Demand-Loading of CSS and SourceMaps.
        new MiniCssExtractPlugin({
          filename: 'styles/[name].css',
          chunkFilename: '[id].css',
        }),

        // A webpack plugin to remove/clean your build folder(s).
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: [path.resolve(process.cwd(), `${basePath}/*`)]
        }),

        // Copies individual files or entire directories, which already exist, to the build directory
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: 'dist/library',
                    to: '.'
                }
            ]
        })
    ],
    // Configuration regarding modules
    module: {
        // Rules for modules (configure loaders, parser options, etc.)
        rules: [
            {
                test: /\.(ts|tsx)$/i,
                loader: 'ts-loader',
                exclude: ['/node_modules/'],
            },
            {
                test: /\.scss$/i,
                exclude: ['/node_modules/'],
                use: [{
                    loader: stylesHandler
                }, {   
                    loader: 'css-loader'
                }, {
                    loader: 'postcss-loader'
                }, {
                    loader: 'sass-loader'
                }],
            },
            {
                test: /\.css$/i,
                exclude: ['/node_modules/'],
                use: [stylesHandler, 'css-loader', 'postcss-loader'],
            },
            {
                test: /\.(?:eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
                type: 'asset/resource'
            },

            // Add your rules for custom modules here
            // Learn more about loaders from https://webpack.js.org/loaders/
        ],
    },
    // Options for resolving module requests
    resolve: {
        // Extensions that are used
        extensions: ['.tsx', '.ts', '.js', '.scss'],
    },
    // A list of name defining all sibling configurations it depends on. Dependent configurations need to be compiled first.
    dependencies: ['numberSequencelibrary']
};

// Chosen mode tells webpack to use its built-in optimizations accordingly. ("production" | "development" | "none")
if (isProduction) {
    webappConfig.mode = 'production';
    webappConfig.plugins.push(new MiniCssExtractPlugin());
} else {
    webappConfig.mode = 'production';
}

module.exports = webappConfig
