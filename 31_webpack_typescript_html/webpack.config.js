// Generated using webpack-cli https://github.com/webpack/webpack-cli

//https://stackoverflow.com/questions/35903246/how-to-create-multiple-output-paths-in-webpack-config
//https://www.loginradius.com/blog/async/write-a-javascript-library-using-webpack-and-babel/
//https://webpack.js.org/concepts/entry-points/
//https://medium.com/@ravelantunes/multi-project-setup-with-webpack-2480e9110fc5

const libraryConfig = require('./webpack.library.config')
const webappConfig = require('./webpack.webapp.config')

module.exports = [ libraryConfig, webappConfig ]