import { NumberSequenceResult } from './numberSequenceResult'
import { WebappInput } from './webappInput'

import './styles/styles.scss'
import mainImageSource from './assets/images/numbers.png'

class Webapp {

    private minimum: number
    private maximum: number
    private selectId: string
    private fibonacciResult: NumberSequenceResult
    private primeNumbersResult: NumberSequenceResult

    constructor() {
        this.minimum = 0
        this.maximum = 10
        this.selectId = ''
        this.fibonacciResult = null
        this.primeNumbersResult = null
    }

    public static createWebapp(webappInput: WebappInput): Webapp {
        const webapp = new Webapp()
        webapp.minimum = webappInput.minimum
        webapp.maximum = webappInput.maximum
        webapp.selectId = webappInput.selectId

        return webapp
    }

    public registerCalculationFibonacciCallback(resultId: string, calculationFibonacciCallback: Function): Webapp {
        this.fibonacciResult = new NumberSequenceResult(resultId, calculationFibonacciCallback)
        return this
    }

    public registerCalculationPrimeNumbersCallback(resultId: string, calculationPrimeNumbersCallback: Function): Webapp {
        this.primeNumbersResult = new NumberSequenceResult(resultId, calculationPrimeNumbersCallback)
        return this
    }

    public initializeDOM(): Webapp {
        const selectElement = document.getElementById(this.selectId)
    
        const mainImage = document.getElementById('mainImage') as HTMLImageElement;
        mainImage.src = mainImageSource;

        for (let i = this.minimum; i <= this.maximum; i++) {
            const optionNode = document.createElement('option')
            optionNode.value = String(i)
            optionNode.innerHTML = String(i)
            
            selectElement.appendChild(optionNode)
        }

        selectElement.addEventListener('change', (event: any) => {
            const value = event.target.value
            
            if (!!this.fibonacciResult)
                this.fibonacciResult.calculateAndPrint(value)
            if (!!this.primeNumbersResult)
                this.primeNumbersResult.calculateAndPrint(value)
        })

        return this
    }
}

export { Webapp }