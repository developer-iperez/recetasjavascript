export interface WebappInput {
    minimum: number
    maximum: number
    selectId: string
}
