export class NumberSequenceResult {

    private resultId: string;
    private callback: Function;

    constructor(resultId: string, callback: Function) {
        this.resultId = resultId;
        this.callback = callback;
    }

    public calculateAndPrint(value: number) {
        const result = this.callback(value);
        this.printResult(result);
    }

    private printResult(result: Array<number>) {
        const resultElement = document.getElementById(this.resultId);
        resultElement.innerHTML = `${result}`;
    }
}
