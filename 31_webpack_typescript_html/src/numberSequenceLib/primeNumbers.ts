//https://codepen.io/thiagomata/pen/OJMYOBe

function isPrime(n: number): boolean {
    if (n === 2)
        return true

    if (n % 2 === 0)
        return false

    for (let i = 3; i <= Math.sqrt(n); i += 2) {
        if(n % i === 0)
            return false
    }

    return true
}

export default function primeNumbers(maximum: number): Array<number> {
    const primes: Array<number> = [];

    for (let i = 0; i <= maximum; i++) {
        if (isPrime(i))
            primes.push(i)
    }

    return primes
}