import fibonacci from './fibonacci'
import primeNumbers from './primeNumbers'

export function calculateFibonacci(value: number): Array<number> {
    if (value < 0)
        throw new Error('Value cannot be negative')

    const fibonacciResult = fibonacci(value)
    return fibonacciResult
}

export function calculatePrimeNumbers(value: number): Array<number> {
    if (value < 0)
        throw new Error('Value cannot be negative')
        
    const primeNumberResults = primeNumbers(value)
    return primeNumberResults
}