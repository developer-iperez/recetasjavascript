export default function fibonacci(value: number): Array<number> {
    const fibonacci: Array<number> = []
    for (let i = 0; i < value; i++) {
        if (fibonacci.length < 2) {
            fibonacci.push(i)
            continue
        }

        fibonacci.push(fibonacci[i - 1] + fibonacci[i - 2]);
    }
    return fibonacci
}