function checkValue(value) {
    console.log(`Value: ${value} [with single negation] -> ${!value}`);
    console.log(`Value: ${value} [with new Boolean] -> ${new Boolean(value)}`);
    console.log(`Value: ${value} [with Boolean] -> ${Boolean(value)}`);
    console.log(`Value: ${value} [with double negation] -> ${!!value}`);
}

// Result -> false
checkValue(undefined)
checkValue(null)
checkValue(+0)
checkValue(-0)
checkValue('')
checkValue(NaN)
checkValue(false)

// Result -> true
checkValue(1)
checkValue('string')
checkValue({})
checkValue([])
