// https://developer.mozilla.org/es/docs/Web/HTML/Elemento/input/email
// HTML5 input type="email"

// https://www.javatpoint.com/javascript-form-validation
// function validateemail()  { ... var atposition=x.indexOf("@"); ... }  

// https://flaviocopes.com/how-to-validate-email-address-javascript/
// HTML5 -> <input type="email" name="email" placeholder="Your email">
// o /\S+@\S+/

// https://tylermcginnis.com/validate-email-address-javascript/
// /^[^\s@]+@[^\s@]+\.[^\s@]+$/

// https://www.w3resource.com/javascript/form/email-validation.php
// /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

// http://zparacha.com/validate-email-address-using-javascript-regular-expression
// /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/

// https://www.codeproject.com/tips/492632/email-validation-in-javascript
// /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/

// https://emailregex.com/
// /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/

// https://www.sitepoint.com/javascript-validate-email-address-regex/
// /^(([^<>()[]\.,;:s@"]+(.[^<>()[]\.,;:s@"]+)*)|(".+"))@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}])|(([a-zA-Z-0-9]+.)+[a-zA-Z]{2,}))$/

// https://www.codespot.org/javascript-email-validation/
// /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

// https://html.form.guide/best-practices/validate-email-address-using-javascript.html
// /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/


function mailValidation(mail) {
    const pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return mail.match(pattern) !== null;
}

let mails = [
    'mail1',
    'mail1.com',
    'mail1@.com',
    '@gmail.com',
    'user@gmail.com',
    'user.name@gmail.com'
];

for (let mail of mails) {
    console.log(`${mail} : ${mailValidation(mail)}`);
}
